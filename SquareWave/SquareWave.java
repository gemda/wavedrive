package SquareWave;
import java.io.BufferedWriter;
import java.io.IOException;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Channel;
import OptionWords.Options;


public class SquareWave extends AbstractWaveForm{
	private static final int codeIdentifier=500000;
	
	private Double amplitude;
	private Double frequencyHz;
	private Double frequencyMs;
	private Double dutyPercent;
	private Double dutyMs;
	private Double durationNumb;
	private Double durationMs;
	
	
	//*******constructors************
	/**
	 *  Creates new SquareWave element, initialize variables
	 */
	public SquareWave(Channel _channel, SquareWindow _squareWindow) {
		super(_channel,_squareWindow,Options.Square);
		
		//System.out.println(this.toString());
	}
	

	//******other functions*************
	/**
	 * Initialize other class members 
	 */
	protected void initializeOtherVariables(){
		//squareWindow=(SquareWindow)abstractWaveTypeWindow;
		
		this.amplitude = ((SquareWindow)abstractWaveTypeWindow).getAmpl();
		this.frequencyHz = ((SquareWindow)abstractWaveTypeWindow).getFreqHz();
		this.frequencyMs = ((SquareWindow)abstractWaveTypeWindow).getFreqMs();
		this.dutyPercent = ((SquareWindow)abstractWaveTypeWindow).getDutyPercent();
		this.dutyMs = ((SquareWindow)abstractWaveTypeWindow).getDutyMs();
		this.durationNumb = ((SquareWindow)abstractWaveTypeWindow).getDurationNumb();
		this.durationMs = ((SquareWindow)abstractWaveTypeWindow).getDurationMs();
		this.baselineabsolute=((SquareWindow)abstractWaveTypeWindow).getBaseabs();
		this.baselinerelative=((SquareWindow)abstractWaveTypeWindow).getBaserel();
		this.comment=((SquareWindow)abstractWaveTypeWindow).getComment();
		
		if(frequencyMs==null)
			frequencyMs=1/frequencyHz*1000;
		
		if(dutyMs==null)
			dutyMs=frequencyMs*dutyPercent/100;
		
		if(durationNumb==null)
			durationNumb=durationMs/frequencyMs;
	}
	
	/**
	 * Calculates real values from the given parameters
	 * the point is that transform equivalent parameters into those which are most fitting to programming
	 * these are: amplitude, frequencyMs, dutyMs, durationNumb
	 * plus correction added to get 1/10 ms resolution
	 */
	public void calculateRealValues(){
		if(amplitude==0){
			for(int i=0;i<=frequencyMs*resolutionCorrection;i++){
				realValues.add(0.0);
			}
		}else{
			
			for(int number=0;number<durationNumb;number++){
				int counter=0;
				for(int cycle=0;cycle<frequencyMs*resolutionCorrection;cycle++){
					if(counter<dutyMs*resolutionCorrection){
						realValues.add(baselineabsolute+amplitude);
					}else{
						realValues.add(baselineabsolute+0.0);
					}
					counter++;
				}
			}
		}
		

	}
	
	/**
	 * ToString method for parameters and realValues
	 */
	@Override
	public String toString() {
		return "SquareWave [amplitude=" + amplitude + ", frequencyHz="
				+ frequencyHz + ", frequencyMs=" + frequencyMs
				+ ", dutyPercentage=" + dutyPercent + ", dutyMs=" + dutyMs
				+ ", durationNumb=" + durationNumb + ", durationMs="
				+ durationMs + ", baseline abs="+baselineabsolute+
				", baseline relative="+baselinerelative+", comment="+comment+"]\n"+realvaluesOut();		 
	}
	
	/**
	 * Creates coded output for Arduino
	 * @throws IOException 
	 */
	public void createOutput(BufferedWriter bw,Double min, Double max,Double res) throws IOException {
		for(int i=0;i<durationNumb;i++){
			bw.write(codeIdentifier+"\n");
			bw.write((int)Math.round(this.dutyMs)+"\n");
			bw.write(convertToPWMValue(baselineabsolute+amplitude, min, max,res)+"\n");
			if(this.dutyPercent!=100){
				bw.write(codeIdentifier+"\n");
				bw.write((int)Math.round(this.frequencyMs-this.dutyMs)+"\n");
				bw.write(convertToPWMValue(baselineabsolute, min, max,res)+"\n");
			}
		}
	};
	
	
	
	//*************getters*******************
	
	public SquareWindow getTypeWindow(){
		return ((SquareWindow)abstractWaveTypeWindow);
	}
	
	public int getCodedLength(){
		return 6*(int)Math.round(this.durationNumb);
	}
}
