package SquareWave;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import AbstractClasses.AbstractWaveTypeWindow;
import DataStructure.Channel;
import Exception.WavedriveException;
import OptionWords.Options;

/*
JSpinner bug:
https://bugs.openjdk.java.net/browse/JDK-4765300
 */
@SuppressWarnings("serial")
public class SquareWindow extends AbstractWaveTypeWindow {
	private Double ampl=null,freqHz=null,freqMs=null,dutyPercent=null,dutyMs=null,durationNumb=null,durationMs=null;

	
	//**************constructor***************
	
	/**
	 * First constructor. Creates new SquareWindow object or perform modify operation. Also shows SquareWindow.
	 */
	public SquareWindow(Channel _channel, String _optionType){
		super(_channel, _optionType,Options.SquareWindow);
		
		setFocusable(true);
	}
	
	/**
	 * Second Constructor. loads SquareWindow object from file
	 */
	public SquareWindow(Channel _channel,String _optionType, BufferedReader br) throws WavedriveException{
		super(_channel, _optionType,Options.SquareWindow,br);
		
		checkAndGetValues();
	}
	
	
	
	//************other functions************
	/**
	 * Read values from active Spinners and check their validity
	 * @modifies: variables ampl,freqHz,freqMs,dutyPercent,dutyMs,durationNumb,durationMs
	 */
	protected void checkAndGetValues() throws WavedriveException{
		
		ampl=objectToDouble(((SquareOptionPanel)abstractOptionPanel).getAmplSpinner().getValue());
		if(ampl==0)
			throw new WavedriveException("Wrong amplitude value!");
		
		if(((SquareOptionPanel)abstractOptionPanel).getFreqChoice1().isSelected()){
			freqHz=objectToDouble(((SquareOptionPanel)abstractOptionPanel).getFreqHzSpinner().getValue());
			if(freqHz==0)
				throw new WavedriveException("Wrong frequency(Hz) value!");
		}else{
			freqMs=objectToDouble(((SquareOptionPanel)abstractOptionPanel).getFreqMsSpinner().getValue());
			if(freqMs==0)
				throw new WavedriveException("Wrong frequency(Ms) value!");
		}
		
		
		if(((SquareOptionPanel)abstractOptionPanel).getDutyChoice1().isSelected()){
			dutyPercent=objectToDouble(((SquareOptionPanel)abstractOptionPanel).getDutyPercentSpinner().getValue());
			if(dutyPercent==0)
				throw new WavedriveException("Wrong duty(%) value!");
			
		}else{
			dutyMs=objectToDouble(((SquareOptionPanel)abstractOptionPanel).getDutyMsSpinner().getValue());
			if(dutyMs==0)
				throw new WavedriveException("Wrong duty(Ms) value!");
		}
		
		if(((SquareOptionPanel)abstractOptionPanel).getDurationChoice1().isSelected()){
			durationNumb=objectToDouble(((SquareOptionPanel)abstractOptionPanel).getDurationNumbSpinner().getValue());
			if(durationNumb==0)
				throw new WavedriveException("Wrong duration(number) value!");
		}else{
			durationMs=objectToDouble(((SquareOptionPanel)abstractOptionPanel).getDurationMsSpinner().getValue());
			if(durationMs==0)
				throw new WavedriveException("Wrong duration(Ms) value!");
		}
		
		
		getBaselineAndComment();
		
	}
	
	/**
	 * Save  SquareWindow with options
	 */
	public void saveWindow(BufferedWriter bw) throws IOException{
		bw.write("Square\n");
		commentPanel.saveCommentPanel(bw);
		((SquareOptionPanel)abstractOptionPanel).saveOptionPanel(bw);
		bw.write("EndSquare\n");
	}
	
	protected void loadWindow(BufferedReader br) throws IOException, WavedriveException{
		String line = null;
		while (!(line = br.readLine()).equals("EndSquare")) {
			if(line.equals("Comment")){
				commentPanel.loadCommentPanel(br);
			}else if(line.equals("Options")){
				((SquareOptionPanel)abstractOptionPanel).loadOptionPanel(br);
			}else{
				throw new WavedriveException("Read unknown data group identifier from textfile: "+line);
			}
		}
	}
	

	
	//*************getters***********************	
	public Double getAmpl() {
		return ampl;
	}
	
	public Double getFreqHz() {
		return freqHz;
	}

	public Double getFreqMs() {
		return freqMs;
	}
	
	public Double getDutyPercent() {
		return dutyPercent;
	}
	
	public Double getDutyMs() {
		return dutyMs;
	}

	public Double getDurationNumb() {
		return durationNumb;
	}
	
	public Double getDurationMs() {
		return durationMs;
	}

	public SquareOptionPanel getOptionPanel() {
		return ((SquareOptionPanel)abstractOptionPanel);
	}
	
	
	
	//*****************action handling*******************
	/**
	 * Performs Buttons and RadioButtons action handling
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			Object obj=e.getSource();
			if(obj.getClass()==JButton.class){
				
				checkAndGetValues();
				channel.add(this);
				
				dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			}
		}catch(WavedriveException e1) {
			System.out.println("Error in SquareWindow frame event handling: "+e1.getMessage());
			
			JOptionPane.showMessageDialog (null, e1.getMessage() ,"Warning", JOptionPane.WARNING_MESSAGE);
			
		}

		
	}


}
