package SquareWave;

import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import AbstractClasses.AbstractOptionPanel;
import Exception.WavedriveException;

@SuppressWarnings("serial")
public class SquareOptionPanel extends AbstractOptionPanel{
	//amplitude
	private JLabel amplLabelName=new JLabel("Amplitude");
	private SpinnerModel amplSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner amplSpinner=new JSpinner(amplSpinnerModell);
	private JLabel amplLabelUnit=new JLabel("mV");
	
	//frequency
	private JLabel freqLabelName=new JLabel("Frequency");
	private JRadioButton freqChoice1=new JRadioButton();
	private SpinnerModel freqHzSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner freqHzSpinner=new JSpinner(freqHzSpinnerModell);
	private JLabel freqHzLabelUnit=new JLabel("Hz");
	private JRadioButton freqChoice2=new JRadioButton();
	private SpinnerModel freqMsSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner freqMsSpinner=new JSpinner(freqMsSpinnerModell);
	private JLabel freqMsLabelUnit=new JLabel("ms");
	private ButtonGroup freqButtonGroup=new ButtonGroup();
	
	//duty cycle
	private JLabel dutyLabelName=new JLabel("Duty cycle");
	private JRadioButton dutyChoice1=new JRadioButton();
	private SpinnerModel dutyPercentSpinnerModell=new SpinnerNumberModel(0.0,0.0,100.0,0.1);
	private JSpinner dutyPercentSpinner=new JSpinner(dutyPercentSpinnerModell);
	private JLabel dutyPercentLabelUnit=new JLabel("%");
	private JRadioButton dutyChoice2=new JRadioButton();
	private SpinnerModel dutyMsSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner dutyMsSpinner=new JSpinner(dutyMsSpinnerModell);
	private JLabel dutyMsLabelUnit=new JLabel("ms");
	private ButtonGroup dutyButtonGroup=new ButtonGroup();
	
	//duration
	private JLabel durationLabelName=new JLabel("Duration");
	private JRadioButton durationChoice1=new JRadioButton();
	private SpinnerModel durationNumbSpinnerModell=new SpinnerNumberModel(0,0,50,1);
	private JSpinner durationNumbSpinner=new JSpinner(durationNumbSpinnerModell);
	private JLabel durationNumbLabelUnit=new JLabel("#");
	private JRadioButton durationChoice2=new JRadioButton();
	private SpinnerModel durationMsSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner durationMsSpinner=new JSpinner(durationMsSpinnerModell);
	private JLabel durationMsLabelUnit=new JLabel("ms");
	private ButtonGroup durationButtonGroup=new ButtonGroup();
	
	
	//***************constructors************
	/**
	  * First constructor. Creates new SquareOptionPanel object
	  */
	public SquareOptionPanel() {
		startingFormation();
		addComponentsAndListeners();
	}
	
	/**
	 * Second constructor for modify operation
	 * Set values and visibility of the original element objects  to appear a window with the same option
	 */
	public SquareOptionPanel(SquareOptionPanel otherSquareOptionPanel){
		this.amplSpinner.setValue(otherSquareOptionPanel.amplSpinner.getValue());
		
		this.freqHzSpinner.setValue(otherSquareOptionPanel.freqHzSpinner.getValue());
		this.freqHzSpinner.setEnabled(otherSquareOptionPanel.freqHzSpinner.isEnabled());
		this.freqMsSpinner.setValue(otherSquareOptionPanel.freqMsSpinner.getValue());
		this.freqMsSpinner.setEnabled(otherSquareOptionPanel.freqMsSpinner.isEnabled());
		this.freqChoice1=otherSquareOptionPanel.freqChoice1;
		this.freqChoice2=otherSquareOptionPanel.freqChoice2;
		
		this.durationMsSpinner.setValue(otherSquareOptionPanel.durationMsSpinner.getValue());
		this.durationMsSpinner.setEnabled(otherSquareOptionPanel.durationMsSpinner.isEnabled());
		this.durationNumbSpinner.setValue(otherSquareOptionPanel.durationNumbSpinner.getValue());
		this.durationNumbSpinner.setEnabled(otherSquareOptionPanel.durationNumbSpinner.isEnabled());
		this.durationChoice1=otherSquareOptionPanel.durationChoice1;
		this.durationChoice2=otherSquareOptionPanel.durationChoice2;
		
		this.dutyMsSpinner.setValue(otherSquareOptionPanel.dutyMsSpinner.getValue());
		this.dutyMsSpinner.setEnabled(otherSquareOptionPanel.dutyMsSpinner.isEnabled());
		this.dutyPercentSpinner.setValue(otherSquareOptionPanel.dutyPercentSpinner.getValue());
		this.dutyPercentSpinner.setEnabled(otherSquareOptionPanel.dutyPercentSpinner.isEnabled());
		this.dutyChoice1=otherSquareOptionPanel.dutyChoice1;
		this.dutyChoice2=otherSquareOptionPanel.dutyChoice2;
		
		copyBaselineOptions(otherSquareOptionPanel);
		
		addComponentsAndListeners();
	}
	
	
	
	//*********save load operations****
	/**
	 * Save options to text file
	 */
	public void saveOtherOptions(BufferedWriter bw) throws IOException{
		bw.write("Amplitude\t"+this.amplSpinner.getValue()+"\n");
		
		bw.write("FrequencChoice1\t"+this.freqChoice1.isSelected()+"\n");
		bw.write("FrequencyHz\t"+this.freqHzSpinner.getValue()+"\n");
		bw.write("FrequencyChoice2\t"+this.freqChoice2.isSelected()+"\n");
		bw.write("FrequencyMs\t"+this.freqMsSpinner.getValue()+"\n");
		
		bw.write("DutyChoice1\t"+this.dutyChoice1.isSelected()+"\n");
		bw.write("DutyPercent\t"+this.dutyPercentSpinner.getValue()+"\n");
		bw.write("DutyChoice2\t"+this.dutyChoice2.isSelected()+"\n");
		bw.write("DutyMs\t"+this.dutyMsSpinner.getValue()+"\n");
		
		bw.write("DurationChoice1\t"+this.durationChoice1.isSelected()+"\n");
		bw.write("DurationNumber\t"+this.durationNumbSpinner.getValue()+"\n");
		bw.write("DurationChoice2\t"+this.durationChoice2.isSelected()+"\n");
		bw.write("DurationMs\t"+this.durationMsSpinner.getValue()+"\n");
	}
	
	/**
	 * Load options from text file
	 */
	public void loadOptionPanel(BufferedReader br) throws IOException, WavedriveException{
		
		String line = null;
		line = br.readLine();//reads header
		while (!(line = br.readLine()).equals("EndOptions")) {
			String[] values = line.split("\\t");
			//System.out.println(values[0]+"\t"+values[1]);
			if(values[0].equals("Amplitude")){
				this.amplSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("FrequencChoice1")){
				this.freqChoice1.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("FrequencyHz")){
				this.freqHzSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("FrequencyChoice2")){
				this.freqChoice2.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("FrequencyMs")){
				this.freqMsSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("DutyChoice1")){
				this.dutyChoice1.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("DutyPercent")){
				this.dutyPercentSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("DutyChoice2")){
				this.dutyChoice2.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("DutyMs")){
				this.dutyMsSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("DurationChoice1")){
				this.durationChoice1.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("DurationNumber")){
				this.durationNumbSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("DurationChoice2")){
				this.durationChoice2.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("DurationMs")){
				this.durationMsSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("BaselineChoice1")){
				this.baselineChoice1.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineReal")){
				this.baselineRelSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("BaselineChoice2")){
				this.baselineChoice2.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineAbs")){
				this.baselineAbsSpinner.setValue(Double.parseDouble(values[1]));
			}else{
				throw new WavedriveException("Read unknow variable from file: "+values[0]);
			}
		}
		
		if(this.freqChoice1.isSelected()){
			this.freqHzSpinner.setEnabled(true);
			this.freqMsSpinner.setEnabled(false);
		}else{
			this.freqHzSpinner.setEnabled(false);
			this.freqMsSpinner.setEnabled(true);
		}
		
		if(this.durationChoice1.isSelected()){
			this.durationNumbSpinner.setEnabled(true);
			this.durationMsSpinner.setEnabled(false);
		}else{
			this.durationNumbSpinner.setEnabled(false);
			this.durationMsSpinner.setEnabled(true);
		}
		
		if(this.dutyChoice1.isSelected()){
			this.dutyPercentSpinner.setEnabled(true);
			this.dutyMsSpinner.setEnabled(false);
		}else{
			this.dutyPercentSpinner.setEnabled(false);
			this.dutyMsSpinner.setEnabled(true);
		}
		
		setBaselineSettingsAfterLoading();
	}
	
	
	
	//********other functions****************
	/**
	 * Add components to Panel, set Layout, add ActionListeners
	 * @modifies: all amplitude, frequency, duty cycle, duration Objects
	 */
	private void addComponentsAndListeners(){
		GridLayout gridLayout=new GridLayout(5,7);
		setLayout(gridLayout);
		gridLayout.setVgap(10);
		
		//amplitude
		this.add(amplLabelName);
		this.add(new JLabel());
		this.add(amplSpinner);
		this.add(amplLabelUnit);
		this.add(new JLabel());
		this.add(new JLabel());
		this.add(new JLabel());
		
		//frequency
		this.add(freqLabelName);
		this.add(freqChoice1);
		freqChoice1.addActionListener(new SquareWindow.RadioButtonActionListener1(freqChoice1,freqHzSpinner,freqMsSpinner));
		this.add(freqHzSpinner);
		this.add(freqHzLabelUnit);
		this.add(freqChoice2);
		freqChoice2.addActionListener(new SquareWindow.RadioButtonActionListener1(freqChoice2,freqMsSpinner,freqHzSpinner));
		this.add(freqMsSpinner);
		this.add(freqMsLabelUnit);
		freqButtonGroup.add(freqChoice1);
		freqButtonGroup.add(freqChoice2);
		
		
		//duty cycle
		this.add(dutyLabelName);
		this.add(dutyChoice1);
		dutyChoice1.addActionListener(new SquareWindow.RadioButtonActionListener1(dutyChoice1,dutyPercentSpinner,dutyMsSpinner));
		this.add(dutyPercentSpinner);
		this.add(dutyPercentLabelUnit);
		this.add(dutyChoice2);
		dutyChoice2.addActionListener(new SquareWindow.RadioButtonActionListener1(dutyChoice2,dutyMsSpinner,dutyPercentSpinner));
		this.add(dutyMsSpinner);
		this.add(dutyMsLabelUnit);
		dutyButtonGroup.add(dutyChoice1);
		dutyButtonGroup.add(dutyChoice2);
		
		//duration
		this.add(durationLabelName);
		this.add(durationChoice1);
		durationChoice1.addActionListener(new SquareWindow.RadioButtonActionListener1(durationChoice1,durationNumbSpinner,durationMsSpinner));
		this.add(durationNumbSpinner);
		this.add(durationNumbLabelUnit);
		this.add(durationChoice2);
		durationChoice2.addActionListener(new SquareWindow.RadioButtonActionListener1(durationChoice2,durationMsSpinner,durationNumbSpinner));
		this.add(durationMsSpinner);
		this.add(durationMsLabelUnit);
		durationButtonGroup.add(durationChoice1);
		durationButtonGroup.add(durationChoice2);
		
		//baseline
		setBaselineComponents();
	}
	
	/**
	 * Set starting formation of RadioButtons and Spinners
	 * Called just in the first constructor
	 */
	private void startingFormation(){
		freqChoice1.setSelected(true);
		freqMsSpinner.setEnabled(false);
		
		dutyChoice1.setSelected(true);
		dutyMsSpinner.setEnabled(false);
		
		durationChoice1.setSelected(true);
		durationMsSpinner.setEnabled(false);
	}
	
	
	
	
	//*****************getters***********************

	public JSpinner getAmplSpinner() {
			return amplSpinner;
		}
	
	public JSpinner getFreqHzSpinner() {
		return freqHzSpinner;
	}
	
	public JSpinner getFreqMsSpinner() {
		return freqMsSpinner;
	}
	
	public JSpinner getDutyPercentSpinner() {
		return dutyPercentSpinner;
	}
	
	public JSpinner getDutyMsSpinner() {
		return dutyMsSpinner;
	}

	public JSpinner getDurationNumbSpinner() {
		return durationNumbSpinner;
	}

	public JSpinner getDurationMsSpinner() {
		return durationMsSpinner;
	}
	
	public JRadioButton getFreqChoice1() {
		return freqChoice1;
	}

	public JRadioButton getDutyChoice1() {
		return dutyChoice1;
	}

	public JRadioButton getDurationChoice1() {
		return durationChoice1;
	}

}
