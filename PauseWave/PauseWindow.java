package PauseWave;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import AbstractClasses.AbstractWaveTypeWindow;
import DataStructure.Channel;
import Exception.WavedriveException;
import OptionWords.Options;

@SuppressWarnings("serial")
public class PauseWindow extends AbstractWaveTypeWindow implements ActionListener{
	private Double duration;
	
	//**************constructor***************
	
	/**
	 * First constructor. Creates new PauseWindow object or perform modify operation. Also shows SquareWindow.
	 */
	public PauseWindow(Channel _channel, String _optionType){
		super(_channel, _optionType,Options.PauseWindow);
	}
	
	/**
	 * Second Constructor. loads SquareWindow object from file
	 */
	public PauseWindow(Channel _channel,String _optionType, BufferedReader br) throws WavedriveException{
		super(_channel, _optionType,Options.PauseWindow,br);
		
		checkAndGetValues();
	}
	
	/**
	 * 	Third constructor for padding operation
	 */
	public PauseWindow(Channel _channel, Double length){
		channel=_channel;
		optionType=Options.LoadAppendElement;
		windowType=Options.PauseWindow;
		
		createPanels();
		
		((PauseOptionPanel)abstractOptionPanel).setDurationSpinner(length);
		
		try {
			checkAndGetValues();
		} catch (WavedriveException e) {
			System.out.println("Problem with padding PauseWindow settings");
			e.printStackTrace();
		}
	}
	
	
	
	
	//************other functions************
	/**
	 * Read values from active Spinners and check their validity
	 */
	protected void checkAndGetValues() throws WavedriveException{
		duration=objectToDouble(((PauseOptionPanel)abstractOptionPanel).getDurationSpinner().getValue());
		
		if(duration==0)
			throw new WavedriveException("Wrong duration(Ms) value!");
		getBaselineAndComment();
	}
	
	/**
	 * Save  SquareWindow with options
	 */
	public void saveWindow(BufferedWriter bw) throws IOException{
		bw.write("Pause\n");
		commentPanel.saveCommentPanel(bw);
		((PauseOptionPanel)abstractOptionPanel).saveOptionPanel(bw);
		bw.write("EndPause\n");
	}
	
	protected void loadWindow(BufferedReader br) throws IOException, WavedriveException{
		String line = null;
		while (!(line = br.readLine()).equals("EndPause")) {
			if(line.equals("Comment")){
				commentPanel.loadCommentPanel(br);
			}else if(line.equals("Options")){
				((PauseOptionPanel)abstractOptionPanel).loadOptionPanel(br);
			}else{
				throw new WavedriveException("Read unknown data group identifier from textfile: "+line);
			}
		}
	}

	
	
	//*************getters***********************	
	public Double getDuration() {
		return duration;
	}

	public PauseOptionPanel getOptionPanel() {
		return ((PauseOptionPanel)abstractOptionPanel);
	}
	
	
	//*****************action handling*******************
	/**
	 * Performs Buttons and RadioButtons action handling
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			Object obj=e.getSource();
			if(obj.getClass()==JButton.class){
				
				checkAndGetValues();
				channel.add(this);
				
				dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			}
		}catch(WavedriveException e1) {
			System.out.println("Error in PauseWindow frame event handling: "+e1.getMessage());
			JOptionPane.showMessageDialog (null, e1.getMessage() ,"Warning", JOptionPane.WARNING_MESSAGE);
		}

		
	}




}
