package PauseWave;

import java.io.BufferedWriter;
import java.io.IOException;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Channel;
import OptionWords.Options;

@SuppressWarnings("serial")
public class PauseWave extends AbstractWaveForm{
	private static final int codeIdentifier=500000;
	
	private Double duration;
	
	//*******constructors************
	/**
	 * Creates new PauseWave element, initialize variables
	 */
	public PauseWave(Channel _channel, PauseWindow _pauseWindow) {
		super(_channel,_pauseWindow,Options.Pause);
		
		//System.out.println(this.toString());
	}
	
	
	
	//******other functions*************
	/**
	 * Initialize other class members 
	 */
	protected void initializeOtherVariables(){
		//squareWindow=(SquareWindow)abstractWaveTypeWindow;
		
		this.duration = ((PauseWindow)abstractWaveTypeWindow).getDuration();
	}
	
	/**
	 * Calculates real values from the given parameters
	 * plus correction added to get 1/10 ms resolution
	 */
	public void calculateRealValues(){
		
		for(int i=0;i<duration*resolutionCorrection;i++){
			realValues.add(baselineabsolute+0.0);
		}

	}

	/**
	 * ToString method for parameters and realValues
	 */
	@Override
	public String toString() {
		return "PauseWave duration=" + duration + ", baseline abs="+baselineabsolute+
				", baseline relative="+baselinerelative+", comment="+comment+"]\n"+realvaluesOut();		 
	}

	/**
	 * Creates coded output for Arduino
	 * @throws IOException 
	 */
	public void createOutput(BufferedWriter bw,Double min, Double max,Double res) throws IOException {
		bw.write(codeIdentifier+"\n");
		bw.write((int)Math.round(this.duration)+"\n");
		bw.write(convertToPWMValue(baselineabsolute, min, max,res)+"\n");
	};
	
	//*************getters*******************
	
	public PauseWindow getTypeWindow(){
		return ((PauseWindow)abstractWaveTypeWindow);
	}
	
	public int getCodedLength(){
		return 3;
	}
}
