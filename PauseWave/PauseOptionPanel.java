package PauseWave;

import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import AbstractClasses.AbstractOptionPanel;
import Exception.WavedriveException;

@SuppressWarnings("serial")
public class PauseOptionPanel extends AbstractOptionPanel{
	private JLabel durationLabelName=new JLabel("Duration");
	private SpinnerModel durationSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner durationSpinner=new JSpinner(durationSpinnerModell);
	private JLabel durationLabelUnit=new JLabel("ms");
	
	//***************constructors*************
	 /**
	  * First constructor. Creates new PauseOptionPanel object
	  */
	public PauseOptionPanel(){
		//startingFormation();
		addComponentsAndListeners();
	}
	
	/**
	 * Second constructor for modify operation
	 * Set values and visibility of the original element objects  to appear a window with the same option
	 */
	public PauseOptionPanel(PauseOptionPanel otherPauseOptionPanel){
		
		this.durationSpinner.setValue(otherPauseOptionPanel.getDurationSpinner().getValue());
		
		copyBaselineOptions(otherPauseOptionPanel);
		addComponentsAndListeners();
	}

	
	//*************save load operations***********************
	/**
	 * Save options to text file
	 */
	public void saveOtherOptions(BufferedWriter bw) throws IOException{
		bw.write("Duration\t"+this.durationSpinner.getValue()+"\n");
	}

	
	/**
	 * Load options from text file
	 */
	public void loadOptionPanel(BufferedReader br) throws IOException, WavedriveException{
		
		String line = null;
		line = br.readLine();//reads header
		System.out.println(line);
		while (!(line = br.readLine()).equals("EndOptions")) {
			String[] values = line.split("\\t");
			if(values[0].equals("Duration")){
				this.durationSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("BaselineChoice1")){
				this.baselineChoice1.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineReal")){
				this.baselineRelSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("BaselineChoice2")){
				this.baselineChoice2.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineAbs")){
				this.baselineAbsSpinner.setValue(Double.parseDouble(values[1]));
			}else {
				throw new WavedriveException("Read unknow variable from file: "+values[0]);
			}
		}
		
		setBaselineSettingsAfterLoading();
	}
	
	
	
	//*************other functions***************
	/**
	 * Add components to Panel, set Layout, add ActionListeners
	 */
	private void addComponentsAndListeners(){
		GridLayout gridLayout=new GridLayout(2,7);
		setLayout(gridLayout);
		gridLayout.setVgap(10);
		
		//duration
		this.add(durationLabelName);
		this.add(durationSpinner);
		this.add(durationLabelUnit);
		this.add(new JLabel());
		this.add(new JLabel());
		this.add(new JLabel());
		this.add(new JLabel());
		
		//baseline
		setBaselineComponents();
	}

	
	
	//**********getters***********
	public JSpinner getDurationSpinner() {
		return durationSpinner;
	}
	
	
	//*-********setters***********
	public void setDurationSpinner(Double length){
		durationSpinner.setValue(length);
	}
}
