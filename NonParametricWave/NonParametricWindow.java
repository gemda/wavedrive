package NonParametricWave;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import AbstractClasses.AbstractWaveTypeWindow;
import DataStructure.Channel;
import Exception.WavedriveException;
import OptionWords.Options;
import ReusableModul.FileChooser;


@SuppressWarnings("serial")
public class NonParametricWindow extends AbstractWaveTypeWindow{
	//private NonParametricOptionPanel optionPanel=(NonParametricOptionPanel)abstractOptionPanel;
	
	public static final String directoryName="\\_Datas\\NonParametricWaves";
	private static FileChooser fileChooser=new FileChooser(Options.Load,directoryName,null);
	
	
	//**************constructors*************
	/**
	 * First constructor. creates new NonParametricWindow object or perform modify operation
	 * @throws WavedriveException 
	 */
	public NonParametricWindow(Channel _channel, String _optionType) throws WavedriveException{
		super(_channel, _optionType,Options.NonParametricWindow);
		
		if(optionType.equals(Options.Modify)){
			NonParametricWindow otherNonParametricWindow=(NonParametricWindow)channel.getSelectedElement().getTypeWindow();
			fileChooser.setFilePath(otherNonParametricWindow.getFilePath());
		}

	}

	/**
	 * Second Constructor. Creates new NonParametricWindow object from file
	 */
	public NonParametricWindow(Channel _channel,String _optionType,BufferedReader br) throws IOException, WavedriveException{
		super(_channel, _optionType,Options.NonParametricWindow,br);
		
		checkAndGetValues();
	}
	
	
	
	//***********other functions***********
	
	@Override
	protected void createButtonPanel(){
		buttonPanel=new JPanel();
		JButton fileChooserButton=new JButton("Choose File");
		JButton generateButton=new JButton("Generate");
		
		fileChooserButton.addActionListener(this);
		fileChooserButton.setActionCommand(Options.ChooseFile);
		buttonPanel.add(fileChooserButton);
		
		generateButton.addActionListener(this);
		generateButton.setActionCommand(Options.Generate);
		buttonPanel.add(generateButton);
		
	}
	
	/**
	 * Read values from active Spinners and check their validity
	 * @modifies: variables baseline
	 */
	protected void checkAndGetValues() throws WavedriveException{
		if(getFilePath()==null)
			throw new WavedriveException("File path is not given");
		
		getBaselineAndComment();
	}
	
	/**
	 * Save  NonParametricWindow with options and file informations
	 */
	@Override
	public void saveWindow(BufferedWriter bw) throws IOException {
		bw.write("NonParametric\n");
		commentPanel.saveCommentPanel(bw);
		((NonParametricOptionPanel)abstractOptionPanel).saveOptionPanel(bw);
		fileChooser.saveFileDatas(bw);
		bw.write("EndNonParametric\n");
	}
	
	protected void loadWindow(BufferedReader br) throws IOException, WavedriveException{
		String line = null;
		
		while (!(line = br.readLine()).equals("EndNonParametric")) {
			if(line.equals("Comment")){
				commentPanel.loadCommentPanel(br);
			}else if(line.equals("Options")){
				((NonParametricOptionPanel)abstractOptionPanel).loadOptionPanel(br);
			}else if(line.equals("File")){
				fileChooser.loadFileDatas(br);
			}else{
				throw new WavedriveException("Read unknown data group identifier from textfile: "+line);
			}
		}
	}
	
	
	
	
	//*********getters*************
	public String getFilePath(){
		return fileChooser.getFilePathAndName();
	}

	public FileChooser getFileChooser(){
		return fileChooser;
	}
	
	
	public NonParametricOptionPanel getOptionPanel() {
		return (NonParametricOptionPanel)abstractOptionPanel;
	}
	
	
	
	//*****************action handling*******************
	/**
	 * Performs  action handling of file selection
	 */	
	public void actionPerformed(ActionEvent action)
	{
		try {
			Object obj=action.getSource();
			if(obj.getClass()==JButton.class ){//&& nonParamtericFileChooser.isFileSelected()){
				String buttonType=action.getActionCommand();
				
					if(buttonType.equals(Options.ChooseFile)){
						fileChooser.appearWindow();
					}else if(buttonType.equals(Options.Generate)){
						checkAndGetValues();
						channel.add(this);
						
						dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
					}else{
						throw new WavedriveException("Not recognizable command");
					}
			}
		} catch (WavedriveException e) {
			System.out.println("Error nonparametric window event handling: "+e.getMessage());
			JOptionPane.showMessageDialog (null, e.getMessage() ,"Warning", JOptionPane.WARNING_MESSAGE);
		}
	}
	
}
