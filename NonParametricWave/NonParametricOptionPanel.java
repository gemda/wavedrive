package NonParametricWave;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import AbstractClasses.AbstractOptionPanel;
import Exception.WavedriveException;

public class NonParametricOptionPanel extends AbstractOptionPanel{
	private static final long serialVersionUID = 1L;
	
	
	//*************constructors*************
	/**
	  * First constructor. Creates new NonParametricOptionPanel object
	  */
	public NonParametricOptionPanel(){
		startingFormation();
		addComponentsAndListeners();
	}
	
	/**
	 * Second constructor for modify operation
	 * Set values and visibility of the original element objects  to appear a window with the same option
	 */
	public NonParametricOptionPanel(NonParametricOptionPanel otherNonParametricOptionPanel){
		copyBaselineOptions(otherNonParametricOptionPanel);
		addComponentsAndListeners();
	}

	
	
	//************load save operations********
	/**
	 * Save other options to text file
	 */
	public void saveOtherOptions(BufferedWriter bw) throws IOException{
	}
	
	/**
	 * Load options from text file
	 */
	public void loadOptionPanel(BufferedReader br) throws IOException, WavedriveException{
		
		String line = null;
		line = br.readLine();//reads header
		while (!(line = br.readLine()).equals("EndOptions")) {
			String[] values = line.split("\\t");
			if(values[0].equals("BaselineChoice1")){
				this.baselineChoice1.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineReal")){
				this.baselineRelSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("BaselineChoice2")){
				this.baselineChoice2.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineAbs")){
				this.baselineAbsSpinner.setValue(Double.parseDouble(values[1]));
			}else{
				throw new WavedriveException("Read unknow variable from file: "+values[0]);
			}
		}
		
		setBaselineSettingsAfterLoading();
	}
	
	
	
	//************other functions***************
	private void startingFormation(){

	}
	
	private void addComponentsAndListeners(){
		
		
		//baseline
		setBaselineComponents();
	}

}
