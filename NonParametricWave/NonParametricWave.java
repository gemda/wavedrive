package NonParametricWave;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Channel;
import OptionWords.Options;

/*
 * A file nev legyen lementve vagy a file tartalma azaz az adatpontok??
 */

public class NonParametricWave extends AbstractWaveForm{
	private LinkedList<Double> originalValues;
	
	private String filePath;
	
	
	//*****************Constructors***************
	/**
	 * Creates new NonParametricWave element, initialize variables
	 */
	public NonParametricWave(Channel _channel, NonParametricWindow _nonParametricWindow) {
		super(_channel,_nonParametricWindow,Options.NonParametric);
		
		
		//System.out.println(this.toString());
	}

	
	
	//****************other functions**************
	/**
	 * Reads data from file selected in file chooser
	 * Parametric waveform instead of this function is the calculateRealvalues()
	 */
	protected void readFile(){
		originalValues=new LinkedList<Double>();
		
		try {
			FileInputStream fis = new FileInputStream(filePath);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
 
			String line = null;
			while ((line = br.readLine()) != null) {
				originalValues.add(Double.parseDouble(line));
			}
 
			br.close();
		} catch (IOException e) {
			System.out.println("Error reading NonParametricWave data file: "+e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Initialize other class members 
	 */
	protected void initializeOtherVariables(){
		
		this.filePath=((NonParametricWindow)abstractWaveTypeWindow).getFilePath();
		
	}
	
	/**
	 *  Calculates real values from the given parameters
	 */
	public void calculateRealValues(){
		System.out.println(originalValues.size());
		for(int i=0;i<originalValues.size();i++){
			realValues.add(baselineabsolute+originalValues.get(i));
		}
	}
	
	/**
	 * Creates coded output for Arduino
	 */
	public void createOutput(BufferedWriter bw,Double min, Double max,Double res) throws IOException {
		for(int i=0;i<realValues.size();i++){
			bw.write(convertToPWMValue(realValues.get(i), min, max,res)+"\n");
		}
	};
	
	
	
	//**************getters*********************
	@Override
	public NonParametricWindow getTypeWindow() {
		return ((NonParametricWindow)abstractWaveTypeWindow);
	}

	@Override
	public String toString() {
		String values="";
		values="[ Filepath="+this.filePath+ ", baseline abs="+baselineabsolute+
				", baseline relative="+baselinerelative+", comment="+comment+"]\n"+realvaluesOut();
		return values;
	}
	
	public int getCodedLength(){
		return realValues.size();
	}
}
