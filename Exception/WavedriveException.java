package Exception;

@SuppressWarnings("serial")
public class WavedriveException extends Exception {
	public WavedriveException () {

    }

    public WavedriveException (String message) {
        super (message);
    }

    public WavedriveException (Throwable cause) {
        super (cause);
    }

    public WavedriveException (String message, Throwable cause) {
        super (message, cause);
    }
}