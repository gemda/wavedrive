package DataStructure;
import java.util.LinkedList;

import AbstractClasses.AbstractWaveForm;
import Chart.Chart;
import PauseWave.PauseWave;
import PauseWave.PauseWindow;


public class Protocol {
	private LinkedList<Channel> channels=new LinkedList<>();
	
	private int maxChannelLength=0;
	private static Integer resolutionBit=12;
	
	//**************other functions**************
	
	/**
	 * Add new empty channel to the chart
	 */
	public Channel createChannel(Chart chart){
		Channel channel=new Channel(chart,channels.size());
		channels.add(channel);
		return channel;
	}
	
	/**
	 * Deletes the selected element
	 */
	public void deleteSelectedElement(){
		Channel selectedChannel=getSelectedChannel();
		selectedChannel.deleteWaveElement();
	}
	
	/**
	 * Removes the selected channel
	 */
	public void removeChannel(){
		Channel selectedChannel=getSelectedChannel();
		/*selectedChannel.deleteElementSelection();
		selectedChannel.deleteChartSelection();
		????????????????*/
		channels.remove(selectedChannel);
	}
	
	/**
	 * Removes all channel 
	 */
	public void removeProtocol(){
		channels.removeAll(channels);
	}
	
	/**
	 * Does padding according to the longest channel. Padding value is the last value of that channel
	 * All channels needed to be the same length.
	 */
	public void paddChannels(){
		maxChannelLength=0;
		for(int i=0;i<channels.size();i++){
			int channelLength=0 ;
			LinkedList<AbstractWaveForm> waveElements= channels.get(i).getWaveElements();
			for(int j=0;j<waveElements.size();j++){
				channelLength+=waveElements.get(j).getRealValues().size();
			}
			
			channels.get(i).setLength(channelLength);
			if(channelLength>maxChannelLength){
				maxChannelLength=channelLength;
			}
		}
		
		for(int i=0;i<channels.size();i++){
			int channelLength=channels.get(i).getlength();
			if(channelLength<maxChannelLength){
				int paddLength=maxChannelLength-channelLength;
				
				PauseWindow pauseWindow=new PauseWindow(channels.get(i),paddLength/((double)AbstractWaveForm.resolutionCorrection));
				channels.get(i).appendWaveElement(new PauseWave(channels.get(i), pauseWindow));
			}
		}
		
		/*for(int i=0;i<channels.size();i++){
			LinkedList<AbstractWaveForm> waveElements= channels.get(i).getWaveElements();
			Double sumSize=0.0;
			for(int j=0;j<waveElements.size();j++){
				sumSize+=waveElements.get(j).getRealValues().size();
			}
			System.out.println(i+"-dik csatorna: "+sumSize);
		}*/
	}

	public void refreshProtocol(){
		for(int i=0;i<channels.size();i++){
			channels.get(i).refreshChannel();
		}
	}
	
	
	
	//**************setters********
	public static void  setResolutionBit(int bit){
		resolutionBit=bit;
	}
	
	
	
	//***************getters*******************
	public Channel getSelectedChannel(){
		for(Channel channel: channels){
			if(channel.isChartSelected()){
				return channel;
			}
		}
		return null;
	}

	public LinkedList<Channel> getChannels(){
		return channels;
	}
	
	public static Integer getResolutionBit(){
		return resolutionBit;
	}
	
	public static Integer getPWMRange(){
		Integer pwmRange=0;
		if(resolutionBit==12){
			pwmRange=4096;
		}else if(resolutionBit==10){
			pwmRange=1024;
		}else if(resolutionBit==8){
			pwmRange=256;
		}
		return pwmRange;
	}
	
}
