package DataStructure;
import java.util.LinkedList;

import AbstractClasses.AbstractWaveForm;
import AbstractClasses.AbstractWaveTypeWindow;
import Chart.Chart;
import NonParametricWave.NonParametricWave;
import NonParametricWave.NonParametricWindow;
import OptionWords.Options;
import PauseWave.PauseWave;
import PauseWave.PauseWindow;
import RampWave.RampWave;
import RampWave.RampWindow;
import SquareWave.SquareWave;
import SquareWave.SquareWindow;

public class Channel {
	
	private LinkedList<AbstractWaveForm> waveElements;
	
	private Chart chart;
	private int id;
	private int length;
	private int channelNumber;
	private boolean selectionState=false;

	private Double minValue;
	private Double maxValue;

	

	//*****************constructors***************
	public Channel(Chart _chart, int _id){
		id=_id;
		chart=_chart;
		waveElements =new LinkedList<>();
	}
	
	
	
    //*********other operations**********************
	public void add(AbstractWaveTypeWindow typeWindow){
		
		String windowType=typeWindow.getTypeOfWindow();
		String optionType=typeWindow.getOptionType();
		
		switch(windowType){
		case Options.SquareWindow:
			
			//TODO instanceof elelnorzese ugy szep..
			SquareWave squareWave=new SquareWave(this,(SquareWindow)typeWindow);
			if(optionType.equals(Options.Append)){
				appendWaveElement(squareWave);
			}else if(optionType.equals(Options.InsertBefore)){
				insertWaveElement(squareWave,false);
			}else if(optionType.equals(Options.Modify)){
				modifyWaveElement(squareWave);
			}else{
				//throw new WavedriveException("Undefined option");
			}
			break;
		case Options.NonParametricWindow:
			
			//TODO instanceof elelnorzese ugy szep..
			NonParametricWave nonParametricWave=new NonParametricWave(this,(NonParametricWindow)typeWindow);
			if(optionType.equals(Options.Append)){
				appendWaveElement(nonParametricWave);
			}else if(optionType.equals(Options.InsertBefore)){
				insertWaveElement(nonParametricWave,false);
			}else if(optionType.equals(Options.Modify)){
				modifyWaveElement(nonParametricWave);
			}else{
				//throw new WavedriveException("Undefined option");
			}
			
			break;
		case Options.RampWindow:
			
			//TODO instanceof elelnorzese ugy szep..
			RampWave rampWave=new RampWave(this,(RampWindow)typeWindow);
			if(optionType.equals(Options.Append)){
				appendWaveElement(rampWave);
			}else if(optionType.equals(Options.InsertBefore)){
				insertWaveElement(rampWave,false);
			}else if(optionType.equals(Options.Modify)){
				modifyWaveElement(rampWave);
			}else{
				//throw new WavedriveException("Undefined option");
			}
			break;
		case Options.PauseWindow:
			
			//TODO instanceof elelnorzese ugy szep..
			PauseWave pauseWave=new PauseWave(this,(PauseWindow)typeWindow);
			if(optionType.equals(Options.Append)){
				appendWaveElement(pauseWave);
			}else if(optionType.equals(Options.InsertBefore)){
				insertWaveElement(pauseWave,false);
			}else if(optionType.equals(Options.Modify)){
				modifyWaveElement(pauseWave);
			}else{
				//throw new WavedriveException("Undefined option");
			}
			break;
		}
		
    	//range adjustments first
    	int startIndex=0;
    	for(AbstractWaveForm element:waveElements){
    		element.setRangeValues(startIndex);
    		startIndex=element.getEndIndex();
    	}
    	
    	chart.makeChartAdjustments();
		
	}
	
	/**
     * Appends element to the end of the data set of the selected channel
     * Call function which make adjustments of ranges and markers
	 */
	public void appendWaveElement(AbstractWaveForm element){
    	waveElements.add(element);
    	chart.makeChartAdjustments();
    }
    
    /**
     * Insert element before the selected element of the selected channel
     * Call function which make adjustments of ranges and markers
     */
    public void insertWaveElement(AbstractWaveForm element, boolean newChannelInsertionInProgress){
		
    	if(waveElements.size()!=0 ){
        	for(int i=0;i<waveElements.size();i++){
        		if(waveElements.get(i).isSelected()){
        			waveElements.add(i,element);

        			break;
        		}
        	}
    	}else{
    		waveElements.add(element);
    	}
    	
    	refreshChannel();
    	
    	//need for proper insertion of channel elements when loading and inserting channel
		if(newChannelInsertionInProgress){
			element.setSelection(true);
		}
    }
   
    /**
     * Modifies wave element
     */
    public void modifyWaveElement(AbstractWaveForm element){
    	for(int i=0;i<waveElements.size();i++){
    		if(waveElements.get(i).isSelected()){
    			waveElements.add(i,element);
    			waveElements.remove(i+1);
    			break;
    		}
    	}
    	
    	refreshChannel();
    	chart.makeChartAdjustments();
    }
    
    /**
     * Deletes selected element of the selected channel
     * Call function which make adjustments of ranges and markers
     */
    public void deleteWaveElement(){
    	waveElements.remove(getSelectedElement());
    	
    	refreshChannel();
    	chart.makeChartAdjustments();
    }

   public LinkedList<AbstractWaveForm> getWaveElements() {
		return waveElements;
	}
   
   public boolean equals(Object other){
	   if(other == null) return false;
	   if(other == this) return true;
	   if(!(other instanceof Channel)) return false;
	   Channel otherChannel = (Channel)other;
	   return otherChannel.id==this.id;
   }
    
   /**
    * iterates through all the elements and calculates again the values correcting the baseline
    * Correction needed because of inserting new element or modifying existing one
   */
	public void refreshChannel(){
    	for(int i=0;i<waveElements.size();i++) {
    		if(i>0){
    			//TODO
    			waveElements.get(i).refreshRealValues(waveElements.get(i-1).getRealValues().getLast());
    			//waveElements.get(i).refreshRealValues(waveElements.get(i-1).getBaselineAbsolute());
    		}else{
    			waveElements.get(i).refreshRealValues(0.0);
    		}
    	}
    	
    	chart.makeChartAdjustments();
	}
   
   
	//****************setters******************
	public void setChartSelection(boolean state){
		this.selectionState=state;
	}
	
	public void setLength(int length){
		this.length=length;
	}
	
	public void setChannelNumber(int channelNumber) {
		this.channelNumber = channelNumber;
	}
	
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}

	public void setName(String name){
		this.chart.setChartName(name);
	}
	
	
	
	//****************getters************
	public boolean isChartSelected(){
		return this.selectionState;
	}
	
	public AbstractWaveForm getSelectedElement(){
		for(AbstractWaveForm waveElement:waveElements){
			if(waveElement.isSelected()){
				return waveElement;
			}
		}
		return null;
	}
	
    public double getAppendAligment(){
 	   if(waveElements.size()!=0){	//the first element
   			int index=waveElements.getLast().getRealValues().size();
   			return waveElements.getLast().getRealValues().get(index-1);
 	   }
 	   return 0;
    }
    
    public double getInsertAligment(){
 	   for(int i=0;i<waveElements.size();i++){
 	   		if(waveElements.get(i).isSelected()){
 	   			if(i!=0){
 	    			int index=waveElements.get(i-1).getRealValues().size();
 	    			System.out.println(waveElements.get(i-1).getRealValues().get(index-1));
 	    			return waveElements.get(i-1).getRealValues().get(index-1);
 	   			}
 			}
 	   }
 	   //if inserted in the first place
 	   return 0;
    }
    
    public int getId(){
    	return this.id;
    }
	
    public int getlength(){
    	return this.length;
    }
    
	public int getChannelNumber() {
		return channelNumber;
	}
    
	public String getName(){
		return chart.getChartName();
	}
	
	public Double getMinValue(){
		return minValue;
	}
	
	public Double getMaxValue(){
		return maxValue;
	}
	
	public Chart getChart(){
		return this.chart;
	}
}
