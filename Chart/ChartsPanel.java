package Chart;

import java.nio.channels.Channel;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import Main.MainWindow;

@SuppressWarnings("serial")
public class ChartsPanel extends JPanel{
	private MainWindow mainWindow;
	
	private LinkedList<Chart> charts;
	
	//***********constructor************
	/**
	 * Constructor of this class
	 */
	public ChartsPanel(MainWindow _mainWindow){
		mainWindow=_mainWindow;
		charts=new LinkedList<Chart>();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	
	
	//**********setters***********************
	/**
	 * Set selection of selected chart
	 */
	public void setSelection(Chart selectedChart){
		for(Chart chart:charts ){
			if(chart.equals(selectedChart)){
				chart.setChartSelection();
			}else{
				chart.deleteChartSelection();
			}
		}
	}
	
	public void removeChart(){
		Chart chart=getSelected();
		charts.remove(chart);
		chart.getChartPanel().setVisible(false);
	}
	
	public void removeAllChart(){
		for(Chart chart:charts ){
			chart.getChartPanel().setVisible(false);
		}
		
		charts.removeAll(charts);
	}
	
	
	
	//**********other functions************
	/**
	 * Add new chart to Panel
	 */
	public Chart addChart(String name){
		Chart chart = new Chart(this,charts.size(),name);
		charts.add(chart);
		mainWindow.pack();
		return chart;
	}
	
	
	
	//**********getters**********************
	
	/**
	 * Returns selected chart
	 */
	public Chart getSelected(){
		for(Chart chart:charts ){
			if(chart.isSelected()){
				return chart;
			}
		}
		return null;
	}
	
	
	/*public LinkedList<Channel> getChannels(){
		return channels;
	}*/
}
