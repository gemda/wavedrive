package Chart;

import java.awt.Color;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.plot.IntervalMarker;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.ValueMarker;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Channel;

@SuppressWarnings("serial")
public class ChartMarker implements Serializable{
	Marker elementSelectionMarker=null; //could be a list for multiple selection
	ArrayList<Marker> rangeMarkers=new ArrayList<>();
	
	Chart chart;
	
	//double chartMouseCoordX=0;
	double chartMouseCoordY=0;
	double chartMouseCoordX=0;
	
	/**
	 * Costructor of this class
	 */
	public ChartMarker(Chart _chart){
		chart=_chart;
	}
	
	/**
	 * Deletes all range markers
	 */
	public void deleteRangeMarkers(){
    	for(Marker element: rangeMarkers){
    		chart.getPlot().removeDomainMarker(element);
     	}
    	rangeMarkers.clear();
	}
	
	/**
	 * Make range marker adjustments of the selected chart
	 */
    public void adjustRangeMarkers(LinkedList<AbstractWaveForm> waveElements){
    	deleteRangeMarkers();
    	
    	int counter=0;
    	for(AbstractWaveForm element: waveElements){
    		Marker rangeMarker = new ValueMarker(element.getEndIndex());
    		rangeMarker.setPaint(Color.green);
    		rangeMarker.setLabel("R"+counter);
            counter++;
            chart.getPlot().addDomainMarker(rangeMarker);
    		rangeMarkers.add(rangeMarker);
    	}
        /*
        start.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
       // start.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
        */
    }
    
    /**
     * Deletes all element selection
     */
    public void deleteElementSelection(LinkedList<AbstractWaveForm> waveElements){
        for(AbstractWaveForm waveForm: waveElements){
        	waveForm.setSelection(false);
        }
        chart.getPlot().removeDomainMarker(elementSelectionMarker);
        elementSelectionMarker=null;
       // chart.setSelectedElement(null);
    }
    
    /**
     * Set element selection according to mouse X Y position
     * The problem with getDomainCrosshairValue() that gives back the closest point compared tot the mouse coordinates
     * So it can be confusing clicking in one range and slecting another...
     * God examples and variations  http://www.jfree.org/phpBB2/viewtopic.php?f=3&t=23810
     */
    public void setElementSelection(ChartMouseEvent event,LinkedList<AbstractWaveForm> waveElements){
    	deleteElementSelection(waveElements);
    	javax.swing.SwingUtilities.invokeLater(new Runnable() { 
	    		public void run() { 
	    			
	    			//make sure that clicking outside the chart deletes the selection because that gives back the same point as before (also double click deletes selection)
					if(chartMouseCoordX!=chart.getPlot().getDomainCrosshairValue()){
						chartMouseCoordX=chart.getPlot().getDomainCrosshairValue();
					  
						for(AbstractWaveForm waveElement: waveElements){
							if(waveElement.getStartIndex()<chartMouseCoordX && waveElement.getEndIndex()>chartMouseCoordX){
								//chart.setSelectedElement(element.getWavefrom());
								waveElement.setSelection(true);
							  	elementSelectionMarker = new IntervalMarker(waveElement.getStartIndex(), waveElement.getEndIndex());
							  	elementSelectionMarker.setPaint(Color.blue);
							  	chart.getPlot().addDomainMarker(elementSelectionMarker);
							}
						}
					} 
	    		}
    		}); 
    	//System.out.println("A: "+channel.plot.getRangeCrosshairValue()); 
		//System.out.println("B: "+channel.plot.getDomainCrosshairValue()); 
		/*double chartX=channel.plot.getDomainCrosshairValue();
		Point2D p = channel.chartPanel.translateScreenToJava2D(event.getTrigger().getPoint());
		Rectangle2D plotArea = channel.chartPanel.getScreenDataArea();
		double chartXX = channel.plot.getDomainAxis().java2DToValue(p.getX(), plotArea, channel.plot.getDomainAxisEdge());
		//double chartY = plot.getRangeAxis().java2DToValue(p.getY(), plotArea, plot.getRangeAxisEdge());
		System.out.println("Amugy chart:"+chartXX);
		System.out.println(event.getTrigger().getX() + " " + event.getTrigger().getY() );*/
    } 
    
    /**
     * Adjust element selection after modifications
     */
    public void adjustElementSelection(Channel channel){

			deleteElementSelection(channel.getWaveElements());
	    	if(channel.getSelectedElement()!=null){
	    	  	elementSelectionMarker = new IntervalMarker(channel.getSelectedElement().getStartIndex(), channel.getSelectedElement().getEndIndex());
	    	  	elementSelectionMarker.setPaint(Color.blue);
	    	  	chart.getPlot().addDomainMarker(elementSelectionMarker);
	    	}
    }
}
