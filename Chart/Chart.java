package Chart;
import java.awt.BasicStroke;
import java.awt.Color;
import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.LinkedList;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Channel;

@SuppressWarnings("serial")
public class Chart extends JPanel implements ChartMouseListener, Serializable{
	private static final int lineWidth=1;
	
	private ChartPanel chartPanel;
	private JFreeChart chart;
	private XYPlot plot;
	private MyXYDataset dataset;
	
	private ChartsPanel chartsPanel;
	private int id;
	String name;
	
	ChartMarker marker;
	
	private Channel channel;
	
	
	
	//****************constructors*****************
	/**
	 * Constructor of this class
	 * Call MyXYDataset to create chart and ChartMarker to create markers
	 */
    public Chart(ChartsPanel _chartsPanel,int _id, String _name) {
    	chartsPanel=_chartsPanel;
    	id=_id;
    	name=_name;
    	
    	//initialize chart
    	dataset=new MyXYDataset(name);
    	
    	chart=ChartFactory.createXYLineChart(null, null, null, dataset);
    	plot = (XYPlot) chart.getPlot();
    	//plot.getRenderer().setSeriesPaint(0, Color.BLACK);
    	plot.getRenderer().setSeriesStroke(0, new BasicStroke(lineWidth));
        chartPanel= new ChartPanel(chart, true, true, true, false, true);
        chartPanel.addChartMouseListener(this);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        
        
        
        chartsPanel.add(chartPanel);
        
        marker=new ChartMarker(this);
    }

    
    
    //********adjustmens************************
    /**
     * Adjust ranges of elements, chart markers and selection appearance
     */
    public void makeChartAdjustments(){
    	LinkedList<AbstractWaveForm> newWaveElements=channel.getWaveElements();
    	
    	int startIndex=0;
    	for(AbstractWaveForm element:newWaveElements){
    		element.setRangeValues(startIndex);
    		startIndex=element.getEndIndex();
    	}
    	
    	marker.adjustRangeMarkers(newWaveElements);
    	marker.adjustElementSelection(channel);
    	dataset.changeDataset(newWaveElements);
    }
    
    
    
    //**********getters************************ 
    public boolean isSelected(){
    	if(chart.isBorderVisible())
    		return true;
    	return false;
    }
    
    public int getId(){
    	return id;
    }
   
	public XYPlot getPlot() {
		return plot;
	}

	public Channel getChannel(){
		return this.channel;
	}

	public ChartPanel getChartPanel(){
		return this.chartPanel;
	}
	
	public String getChartName(){
		return this.name;
	}
	
	
	
	//**********setters************************
    public void setChannel(Channel _channel){
    	channel=_channel;
    }
	
	public void setChartSelection(){
		channel.setChartSelection(true);
		chart.setBorderPaint(Color.YELLOW); //sets border color to BLUE
		chart.setBorderStroke(new BasicStroke(15)); //set line width to 15
    	chart.setBorderVisible(true);
    }
    
    public void deleteChartSelection(){
    	marker.deleteElementSelection(channel.getWaveElements());
    	channel.setChartSelection(false);
    	chart.setBorderVisible(false);
    }
  
    public boolean equals(Object other){
        if(other == null) return false;
        if(other == this) return true;
        if(!(other instanceof Chart)) return false;
        Chart otherChart = (Chart)other;
        return otherChart.id==this.id;
    }
    
    public void setChartName(String name){
    	this.name=name;
    	dataset.setDatasetName(name);
    	try {
			dataset.validateObject();
		} catch (InvalidObjectException e) {
			e.printStackTrace();
		}
    }
    
    
    //***********event handler******************
    /**
     * Handles mouse clicking events on chart, selection of element and chart
     */
    @Override
    public void chartMouseClicked(ChartMouseEvent event) {
    	  //ChartEntity entity = event.getEntity();
    	
    	  chartsPanel.setSelection(this);

    	  marker.setElementSelection(event,channel.getWaveElements());
    	 
    }
    
    @Override
    public void chartMouseMoved(ChartMouseEvent arg0) {}
}
