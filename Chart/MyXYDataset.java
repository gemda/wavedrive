package Chart;

import java.util.LinkedList;
import java.util.Random;

import org.jfree.data.DomainOrder;
import org.jfree.data.xy.AbstractXYDataset;

import AbstractClasses.AbstractWaveForm;

@SuppressWarnings("serial")
class MyXYDataset extends AbstractXYDataset{
	Random random = new Random();
    double[] xValues = new double[0];
    double[] yValues = new double[0];
    
    String name;
    
    /**
     * Constructor of this class
     */
    public MyXYDataset(String _name)
    {
    	name=_name;
    }
    
    /**
     * Changes chart data set according to input
     * @input: waveElements
     */
    public  void  changeDataset(LinkedList<AbstractWaveForm> waveElements){
    	double[] newxValues;
    	double[] newyValues;
    	if(waveElements.size()>0){	//usefull when you delete the last element
        	int lengthOfChart=waveElements.getLast().getEndIndex();
        	newxValues = new double[lengthOfChart];
        	newyValues = new double[lengthOfChart];
        	
        	for(AbstractWaveForm element: waveElements){
        		int index=element.getStartIndex();
        		for(int i=0;i<element.getRealValues().size();i++){
        			newxValues[index]=index;
        			newyValues[index]=element.getRealValues().get(i);
        			index++;
        		}
        	}
    	}else{
        	newxValues = new double[0];
        	newyValues = new double[0];
    	}


        xValues = newxValues;
        yValues = newyValues;
        fireDatasetChanged();
    }
    
    public DomainOrder getDomainOrder()
    {
        return DomainOrder.ASCENDING;
    }
    
    public double getXValue(int series, int item)
    {
        return xValues[item];
    }
    
    public double getYValue(int series, int item)
    {
        return yValues[item];
    }
    
    public int getSeriesCount()
    {
        return 1;
    }
    
    public Comparable getSeriesKey(int series){
    	return name;
    }
    
    public Number getX(int series, int item)
    {
        return new Double(getXValue(series, item));
    }
    
    public Number getY(int series, int item)
    {
        return new Double(getYValue(series, item));
    }
    
    public int getItemCount(int series)
    {
        return xValues.length;
    }

    public void setDatasetName(String name){
    	this.name=name;
    }
 }
    
