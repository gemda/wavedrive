package LoadSave;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import AbstractClasses.AbstractWaveForm;
import Chart.Chart;
import Chart.ChartsPanel;
import DataStructure.Channel;
import DataStructure.Protocol;
import Exception.WavedriveException;
import NonParametricWave.NonParametricWave;
import NonParametricWave.NonParametricWindow;
import OptionWords.Options;
import PauseWave.PauseWave;
import PauseWave.PauseWindow;
import RampWave.RampWave;
import RampWave.RampWindow;
import ReusableModul.FileChooser;
import SquareWave.SquareWave;
import SquareWave.SquareWindow;

public class LoadSaveModul {
	public static final String directoryName="\\_Datas\\SavedObjects";
	public static final String elementExtension="WDE";
	public static final String channelExtension="WDC";
	public static final String protocolExtension="WDP";
	
	private FileChooser fileChooser;
	
	File selectedFile;
	Protocol protocol;
	ChartsPanel chartsPanel;
	
	String optionType;
	
	
	//*************constructor**************
	public LoadSaveModul(ChartsPanel _chartsPanel, Protocol _protocol, String _optionType ){
		protocol=_protocol;
		optionType=_optionType;
		chartsPanel=_chartsPanel;
		
		try{
			if(optionType.contains(Options.Load)){
				
				fileChooser=new FileChooser(Options.Load,directoryName,getExtension());
				
			}else if(optionType.contains(Options.Save)){
				
				fileChooser=new FileChooser(Options.Save,directoryName,getExtension());
				
			}else{
				throw new WavedriveException("Unknown operation type");
			}
			
			fileChooser.appearWindow();
		
			
			if(fileChooser.isFileSelected()){
				if(optionType.contains(Options.Load)){
					
					loadWaveform(optionType);
					
				}else if(optionType.contains(Options.Save)){
					
					saveWaveform(optionType);
					
				}else{
					throw new WavedriveException("Unknown operation type");
				}  
			}
		}catch (WavedriveException e1) {
			System.out.println("Error in load/save operation: "+e1.getMessage());
			e1.printStackTrace();
		}
		
	}
	
	
	
	//**************load save operation handler*******************
	private String getExtension(){
		String extension="";
		try{
			if(optionType.equals(Options.SaveElement) || optionType.equals(Options.LoadAppendElement) || 
					optionType.equals(Options.LoadInsertElement)){
				extension=elementExtension;
			}else if(optionType.equals(Options.SaveChannel) || optionType.equals(Options.LoadAppendChannel) || 
					optionType.equals(Options.LoadInsertChannel)){
				extension=channelExtension;
			}else if(optionType.equals(Options.SaveProtocol) || optionType.equals(Options.LoadProtocol)){
				extension=protocolExtension;
			}else{
				throw new WavedriveException("Unknown option type");
			}
		}catch(WavedriveException e){
			System.out.println("Problem in prefix selection"+e.getMessage());
			e.printStackTrace();
		}
		
		return extension;
	}
	
    /**
     * Load Element or Channel or Protocol depending on the option type
     */
	private void loadWaveform(String optionType){
		BufferedReader br=null;
    	try{
    		
    		String fname=fileChooser.getFilePathAndName();
    		br = new BufferedReader(new FileReader(fname));
    		System.out.println(optionType);
	        if( optionType.equals(Options.LoadAppendElement)){
	        	loadAppendElement(br,protocol.getSelectedChannel());
	        }else if(optionType.equals(Options.LoadInsertElement)){
	        	loadInsertElement(br,protocol.getSelectedChannel(),false);
	        } else if(optionType.equals(Options.LoadAppendChannel)) {
	        	Channel channel=protocol.getSelectedChannel();
	        	if(channel.getWaveElements().size()==0){
	        		loadAppendChannel(br,channel,true);
	        	}else{
	        		loadAppendChannel(br,protocol.getSelectedChannel(),false);
	        	}
	        }else if(optionType.equals(Options.LoadInsertChannel)) {
	        	loadInsertChannel(br,protocol.getSelectedChannel());
	        }else if(optionType.equals(Options.LoadProtocol)){
	        	loadProtocol(br);
	        }
    	} catch (Exception e) {
    		e.printStackTrace();
    		System.out.println("Problem with file "+fileChooser.getFilePath()+"\t"+e.getMessage());
    	}finally {
		  if (null != br) {
		       try {
		    	   br.close();
		    	   br = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
		   }
    	}
	}
	
    /**
     * Save Element or Channel or Protocol depending on the option type
     */
	private void saveWaveform(String optionType) throws WavedriveException{
		  BufferedWriter bw=null;
	    	try{
	    		
	    		String fname=fileChooser.getFilePath()+"\\"+fileChooser.getFileName()+"."+getExtension();
	    		
	    		bw = new BufferedWriter(new FileWriter(fname));
		        if(optionType.equals(Options.SaveElement)){
		        	saveElement(bw,protocol.getSelectedChannel().getSelectedElement(),0,true);
		        }else if(optionType.equals(Options.SaveChannel)){
		        	saveChannel(bw,protocol.getSelectedChannel());
		        }else if(optionType.equals(Options.SaveProtocol)){
		        	saveProtocol(bw);
		        }else{
		        	throw new WavedriveException("Save option preconditions failed");
		        }
		        
		} catch (IOException e) {
		        e.printStackTrace();
		}finally {
			  if (null != bw) {
			       try {
			    	   bw.flush();
			    	   bw.close();
			    	   bw = null;
					} catch (IOException e) {
						e.printStackTrace();
					}
			   }
		}
	}
	
	
	
	
	//**************save operations*******************
	/**
	 * Save selected element
	 */
	public void saveElement(BufferedWriter bw,AbstractWaveForm waveElement,int serialNumber,boolean resetBaselin) throws IOException{
		if(resetBaselin) waveElement.resetBaseline();	//needed to set baseline to rel=0 to be well aligned when loading it
		bw.write("Element "+serialNumber+" ******\n");
		waveElement.getTypeWindow().saveWindow(bw);
		bw.write("EndElement\n");
	}
	
	/**
	 * Save selected channel
	 */
	public void saveChannel(BufferedWriter bw, Channel channel) throws IOException {
    	LinkedList<AbstractWaveForm> waveElements=channel.getWaveElements();
    	bw.write("Channel\t"+channel.getId()+"*****************************\n");
    	bw.write("ChannelProperties"+"\n");
    	bw.write("Name\t"+channel.getName()+"\n");
    	bw.write("MinimumValue\t"+channel.getMinValue()+"\n");
    	bw.write("MaximumValue\t"+channel.getMaxValue()+"\n");
    	bw.write("EndChannelProperties"+"\n");
    	for(int i=0;i<waveElements.size();i++){
    		saveElement(bw,waveElements.get(i),i,false);
    	}
    	bw.write("EndChannel\n");
	}
	
	/**
	 * Save protocol
	 */
	public void saveProtocol(BufferedWriter bw) throws IOException{
		LinkedList<Channel> channels=protocol.getChannels();
		bw.write("Protocol\n");
    	for(Channel channel:channels){
    		saveChannel(bw,channel);
    	}
    	bw.write("EndProtocol\n");
	}
	
	
	
	//*************load operations************************
	/**
	 * Load and append element
	 * @throws WavedriveException 
	 */
	public void loadAppendElement(BufferedReader br,Channel channel) throws IOException, WavedriveException{
    	String line=null;
    	
			line=br.readLine();	//element identifier
			line=br.readLine();//element type
	    	if(line.equals(Options.Square)){
	    		
	    		SquareWindow squareWindow=new SquareWindow(channel,optionType, br);
	    		channel.appendWaveElement(new SquareWave(channel,squareWindow));
	    		
	    	}else if(line.equals(Options.Ramp)){
	    		
	    		RampWindow rampWindow=new RampWindow(channel,optionType, br);
	    		channel.appendWaveElement(new RampWave(channel,rampWindow));
	    		
	    	}else if(line.equals(Options.NonParametric)){
	    		
	    		NonParametricWindow nonParametricWindow=new NonParametricWindow(channel,optionType, br);
	    		channel.appendWaveElement(new NonParametricWave(channel,nonParametricWindow));
	    		
	    	}else if(line.equals(Options.Pause)){
	    		
	    		PauseWindow pauseWindow=new PauseWindow(channel,optionType, br);
	    		channel.appendWaveElement(new PauseWave(channel,pauseWindow));
	    		
	    	}else{
	    		throw new WavedriveException("Read unknown wave type from text file: "+line);
	    	}
	    	line=br.readLine();//EndElement*/
	}
	
	/**
	 * Load and insert element
	 * @throws WavedriveException 
	 */
	public void loadInsertElement(BufferedReader br,Channel channel, boolean newChannelInsertionInProgress) throws IOException, WavedriveException{
	
		String line=null;
		line=br.readLine();	//element identifier
		line=br.readLine();//element type
    	if(line.equals(Options.Square)){
    		
    		SquareWindow squareWindow=new SquareWindow(channel,optionType, br);
    		channel.insertWaveElement(new SquareWave(channel,squareWindow),newChannelInsertionInProgress);
    		
    	}else if(line.equals(Options.Ramp)){
    		
    		RampWindow rampWindow=new RampWindow(channel,optionType, br);
    		channel.insertWaveElement(new RampWave(channel,rampWindow),newChannelInsertionInProgress);
    		
    	}else if(line.equals(Options.NonParametric)){
    		
    		NonParametricWindow nonParametricWindow=new NonParametricWindow(channel,optionType, br);
    		channel.insertWaveElement(new NonParametricWave(channel,nonParametricWindow),newChannelInsertionInProgress);
    		
    	}else if(line.equals(Options.Pause)){
    		
    		PauseWindow pauseWindow=new PauseWindow(channel,optionType, br);
    		channel.insertWaveElement(new PauseWave(channel,pauseWindow),newChannelInsertionInProgress);

    	}else{
    		throw new WavedriveException("Read unknown wave type from text file: "+line);
    	}
    	line=br.readLine();//EndElement
	}
	
	/**
	 * Load channel
	 * @throws WavedriveException 
	 */
	public void loadAppendChannel(BufferedReader br,Channel channel,boolean setChannelProperties) throws IOException, WavedriveException{

		String  line=br.readLine();	//channel identifier
		line=br.readLine();	//channel properties identifier
		while (!(line = br.readLine()).equals("EndChannelProperties")) {
			String[] values = line.split("\\t");
			if(values[0].equals("Name")){
				if(setChannelProperties)channel.setName(values[1]);
			}else if(values[0].equals("MinimumValue")){
				if(setChannelProperties)channel.setMinValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("MaximumValue")){
				if(setChannelProperties)channel.setMaxValue(Double.parseDouble(values[1]));
			}else {
				throw new WavedriveException("Read unknow variable from file: "+values[0]);
			}
		}
		
		br.mark(1000);
		while(!(line = br.readLine()).equals("EndChannel")) {
			br.reset();
			loadAppendElement(br,channel);
			br.mark(1000);
		}
	}
	
	public void loadInsertChannel(BufferedReader br,Channel channel) throws IOException, WavedriveException{

		String  line=br.readLine();	//channel identifier
		line=br.readLine();	//channel properties identifier
		while (!(line = br.readLine()).equals("EndChannelProperties")) {
			String[] values = line.split("\\t");
			if(values[0].equals("Name")){
				//channel.setName(values[1]);
			}else if(values[0].equals("MinimumValue")){
				//channel.setMinValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("MaximumValue")){
				//channel.setMaxValue(Double.parseDouble(values[1]));
			}else {
				throw new WavedriveException("Read unknow variable from file: "+values[0]);
			}
		}
	
		br.mark(1000);
		do {
			br.reset();
			loadInsertElement(br,channel,true);
			br.mark(1000);
		}while(!(line = br.readLine()).equals("EndChannel")); //element identifier or endchannel
		
		//correction of insertWaveElement function effect
		for(AbstractWaveForm waveElement: channel.getWaveElements()){
			waveElement.setSelection(false);
		}
		
	}
	
	/**
	 * Load protocol
	 * @throws WavedriveException 
	 */
	public void loadProtocol(BufferedReader br)throws IOException, WavedriveException{
		String line=br.readLine();	//protocol identifier
		br.mark(1000);
		do {
			br.reset();
			
			Chart chart=chartsPanel.addChart("");
			Channel channel=protocol.createChannel(chart);
			chart.setChannel(channel);
			
			loadAppendChannel(br,channel,true);
			br.mark(1000);
		}while(!(line = br.readLine()).equals("EndProtocol"));
	}
}
