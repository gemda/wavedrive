package temp;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Protocol;
import OptionWords.Options;

@SuppressWarnings("serial")
public class ProtocolPropertiesWindow extends JFrame implements ActionListener{
	private JPanel propertiesPanel=new JPanel();
	
	public Object lock=new Object(); 
	
	private JComboBox<Integer> comboboxSelectPWMResolutions;
	private JButton buttonSetPWMResolution;
	
	private JComboBox<Integer> comboboxSelectChartResolutions;
	private JButton buttonSetChartResolution;
	
	private JComboBox<String> comboboxSelectChannel;
	private JButton buttonModifyChannelProperties;
	
	private Protocol protocol;
	
	public ProtocolPropertiesWindow(Protocol _protocol){
		protocol=_protocol;
		
		createPropertiesPanel();
		
		setWindowClosingOperation();
		setTitle("Set protocol properties");
		setVisible(true);
		pack();
	}
	
	/**
	 * Set window closing operation. Notify other frame waiting for this one
	 */
	public void setWindowClosingOperation(){
		this.addWindowListener(new WindowAdapter() {
	         @Override
	         public void windowClosing(WindowEvent evt) {
	             synchronized (lock) {
	                 setVisible(false);
	                 dispose();
	                 lock.notify();
	                 
	             }
	         }
	      });
	}
	
	public void createPropertiesPanel(){
		GridLayout gridLayout = new GridLayout(3,2);
		propertiesPanel.setLayout(gridLayout);
		gridLayout.setHgap(10);
		gridLayout.setVgap(10);
		
		propertiesPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Protocol operations"));
		
		//chart resolution
		Integer[] resolutionChoices1 = { 10,20};
		comboboxSelectChartResolutions= new JComboBox<Integer>(resolutionChoices1);
		propertiesPanel.add(comboboxSelectChartResolutions);
		
		buttonSetChartResolution=new JButton("Set chart resolution");
		buttonSetChartResolution.setActionCommand(Options.SetChartRes);
		buttonSetChartResolution.addActionListener(this);
		add(buttonSetChartResolution);
		propertiesPanel.add(buttonSetChartResolution);
		
		//PWM resolution
		Integer[] resolutionChoices2 = { 12,10,8};
		comboboxSelectPWMResolutions= new JComboBox<Integer>(resolutionChoices2);
		propertiesPanel.add(comboboxSelectPWMResolutions);
		
		buttonSetPWMResolution=new JButton("Set PWM bit resolution");
		buttonSetPWMResolution.setActionCommand(Options.SetPWMRes);
		buttonSetPWMResolution.addActionListener(this);
		add(buttonSetPWMResolution);
		propertiesPanel.add(buttonSetPWMResolution);
		
		//channels
		int numberOfChannels=protocol.getChannels().size();
		
		String[] channelChoices = new String[numberOfChannels];
		for(int i=0;i<numberOfChannels;i++){
			channelChoices[i]=protocol.getChannels().get(i).getName();
		}
		comboboxSelectChannel=new JComboBox<String>(channelChoices);
		propertiesPanel.add(comboboxSelectChannel);
		
		buttonModifyChannelProperties=new JButton("Modify channel properties");
		buttonModifyChannelProperties.setActionCommand(Options.Modify);
		buttonModifyChannelProperties.addActionListener(this);
		add(buttonModifyChannelProperties);
		propertiesPanel.add(buttonModifyChannelProperties);
		
		add(propertiesPanel);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object obj=e.getSource();
		if(obj.getClass()==JButton.class){
			String optionType=e.getActionCommand();
			
			if(optionType.equals(Options.SetChartRes)){
				
				AbstractWaveForm.setResolution(Integer.parseInt(comboboxSelectChartResolutions.getSelectedItem().toString()));
				protocol.refreshProtocol();
				
			}else if(optionType.equals(Options.SetPWMRes)){
				
				Protocol.setResolutionBit(Integer.parseInt(comboboxSelectPWMResolutions.getSelectedItem().toString()));
				
			}else if(optionType.equals(Options.Modify)){
				
				Thread thread = new Thread() {
			        public void run() {
						int index=comboboxSelectChannel.getSelectedIndex();
						ChannelPropertiesWindow channelPropertiesWindow=new ChannelPropertiesWindow(protocol.getChannels().get(index));
			        	setEnabled(false);
	        			
			            synchronized(channelPropertiesWindow.lock) {
		                    try {
		                    	channelPropertiesWindow.lock.wait();
		                    } catch (InterruptedException e) {
		                        e.printStackTrace();
		                    }
		                    setEnabled(true);
		                    requestFocus();
		                    
			           }
			        }
				};
				thread.start();
			}
			
		}
	}
    

}
