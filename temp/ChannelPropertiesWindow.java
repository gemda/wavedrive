package temp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import Chart.Chart;
import Chart.ChartsPanel;
import DataStructure.Channel;
import DataStructure.Protocol;
import Exception.WavedriveException;
import OptionWords.Options;

@SuppressWarnings("serial")
public class ChannelPropertiesWindow extends JFrame implements ActionListener{
	private ChartsPanel chartsPanel;
	private Protocol protocol;
	
	public Object lock=new Object();
	
	private JPanel mainPanel=new JPanel();
	private JPanel buttonPanel=new JPanel();
	private JPanel optionPanel=new JPanel();
	
	private JLabel chanelNameLabelName=new JLabel("Channel Name");
	private JTextArea textArea=new JTextArea("");
	
	private JLabel minLabelName=new JLabel("Minimum Value");
	private SpinnerModel minSpinnerModell=new SpinnerNumberModel(0.0,-10000.0,10000.0,0.1);
	private JSpinner minSpinner=new JSpinner(minSpinnerModell);
	private JLabel minLabelUnit=new JLabel("mV");
	
	private JLabel maxLabelName=new JLabel("Maximum Value");
	private SpinnerModel maxSpinnerModell=new SpinnerNumberModel(0.0,-10000.0,10000.0,0.1);
	private JSpinner maxSpinner=new JSpinner(maxSpinnerModell);
	private JLabel maxLabelUnit=new JLabel("mV");
	
	
	private Double minValue=null, maxValue=null;
	private String channelName=null;
	private Integer serialNumb=null;
	
	Channel channel;
	Chart chart;
	
	boolean createNewChart=false;
	
	
	//*****************constructor***************
	public ChannelPropertiesWindow(ChartsPanel _chartsPanel,Protocol _protocol){
		chartsPanel=_chartsPanel;
		protocol=_protocol;
		serialNumb=protocol.getChannels().size();
		
		createPanels();
		startingFormation();
		
		createNewChart=true;
		
		setWindowClosingOperation();
		setTitle("Set channel properties");
		setVisible(true);
		pack();
	}
	
	public ChannelPropertiesWindow(Channel _channel){
		channel=_channel;
		chart=channel.getChart();
		
		createPanels();
		this.textArea.setText(channel.getName());
		this.minSpinner.setValue(channel.getMinValue());
		this.maxSpinner.setValue(channel.getMaxValue());


		setWindowClosingOperation();
		setTitle("Modify channel properties");
		setVisible(true);
		pack();
	}
	
	//**************other functions**************
	/**
	 * Set window closing operation. Notify other frame waiting for this one
	 */
	public void setWindowClosingOperation(){
		this.addWindowListener(new WindowAdapter() {
	         @Override
	         public void windowClosing(WindowEvent evt) {
	             synchronized (lock) {
	                 setVisible(false);
	                 dispose();
	                 lock.notify();
	             }
	         }
	      });
	}
	
	/**
	 * Set starting values
	 */
	private void startingFormation(){
		textArea.setText(serialNumb+". channel");
		minSpinner.setValue(0.0);
		maxSpinner.setValue(100.0);
	}
	
	/**
	 * Create panel
	 */
	public void createPanels(){
		//setLayout(new BorderLayout());
		setContentPane(mainPanel);
		
		createButtonPanel();
		createOptionPanel();
		
		mainPanel.add(optionPanel, BorderLayout.NORTH);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);
	}
	
	/**
	 * Organize panel for generate button
	 */
	public void createButtonPanel(){
		JButton generateButton=new JButton("Generate");
		generateButton.setActionCommand(Options.Generate);
		
		generateButton.addActionListener(this);
		buttonPanel.add(generateButton);
	}
	
	/**
	 * Organize panel for operation fields
	 */
	public void createOptionPanel(){
		GridLayout gridLayout = new GridLayout(3,3);
		optionPanel.setLayout(gridLayout);
		gridLayout.setVgap(10);
		
		optionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Channel settings"));
		
		optionPanel.add(chanelNameLabelName);
		optionPanel.add(textArea);
		optionPanel.add(new JLabel());
		
		optionPanel.add(minLabelName);
		optionPanel.add(minSpinner);
		optionPanel.add(minLabelUnit);
		
		optionPanel.add(maxLabelName);
		optionPanel.add(maxSpinner);
		optionPanel.add(maxLabelUnit);
	}
	
	
	
	
	//***************action handling******************
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			Object obj=e.getSource();
			if(obj.getClass()==JButton.class){
				
				minValue=Double.parseDouble(minSpinner.getValue().toString());
				maxValue=Double.parseDouble(maxSpinner.getValue().toString());
				channelName=textArea.getText();
				if(maxValue<=minValue)
					throw new WavedriveException("Problem with min and max value");
				
				if(createNewChart){
					chart=chartsPanel.addChart("");
					channel=protocol.createChannel(chart);
					chart.setChannel(channel);
				}
				chart.setChartName(channelName);
				channel.setMinValue(minValue);
				channel.setMaxValue(maxValue);	
			}
		}catch(WavedriveException e1) {
			System.out.println("Error in ChannelPropertiesWindow frame event handling: "+e1.getMessage());
		}
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
}
