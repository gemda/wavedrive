package OptionWords;

public class Options {
	 public static final String Square = "Square";
	 public static final String Ramp = "Ramp";
	 public static final String NonParametric = "NonParametric";
	 public static final String Pause = "Pause";
	 
	 public static final String SquareWindow = "SquareWindow";
	 public static final String RampWindow = "RampWindow";
	 public static final String NonParametricWindow = "NonParametricWindow";
	 public static final String PauseWindow = "PauseWindow";
	 
	 public static final String AddChannel = "AddChannel";
	 public static final String LoadInsertChannel = "LoadInsertChannel";
	 public static final String LoadAppendChannel = "LoadAppendChannel";
	 
	 public static final String Append = "Append";
	 public static final String InsertBefore = "InsertBefore";
	 public static final String Modify = "Modify";
	 public static final String SetChartRes = "SetChartRes";
	 public static final String SetPWMRes = "SetPWMRes";
	 
	 public static final String Send = "Send";
	 public static final String Generate = "Generate";
	 public static final String ChooseFile = "ChooseFile";
	 public static final String Settings = "Settings";
	 
	 public static final String DeleteElement = "DeleteElement";
	 public static final String DeleteChannel = "DeleteChannel";
	 public static final String DeleteProtocol = "DeleteProtocol";
	 
	 public static final String SaveElement = "SaveElement";
	 public static final String SaveChannel = "SaveChannel";
	 public static final String SaveProtocol = "SaveProtocol";
	 
	 public static final String LoadInsertElement = "LoadInsertElement";
	 public static final String LoadAppendElement = "LoadAppendElement";
	 public static final String LoadProtocol = "LoadProtocol";
	 
	 public static final String Load = "Load";
	 public static final String Save = "Save";
	 public static final String CancelSelection = "CancelSelection";
	 public static final String ApproveSelection = "ApproveSelection";
}


