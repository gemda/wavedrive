package ReusableModul;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import LoadSave.LoadSaveModul;
import Main.MainWindow;
import OptionWords.Options;

@SuppressWarnings("serial")
public class FileChooser extends JFrame implements ActionListener {
	public String modulDirectory=MainWindow.startingDirectory;
	
	
	private JFileChooser fileChooser;
	private String selectedFilename=null;
	private String selectedFilePath=null;
	private String selectedFilePathAndName=null;
	
	boolean fileSelected=false;
	
	String optionType;
	
	class PrefixDrawingFileFilter extends FileFilter {

	    String prefix;
	    String[] suffixes = {"txt"};

	    PrefixDrawingFileFilter(String prefix) {
	        this.prefix = prefix;
	    }

	    public boolean accept(File f) {
	        if (f.isDirectory()) return true;

	        String name = f.getName();
	        if ( name.startsWith(prefix) ) {
	            for (String type : suffixes) {
	                if (name.endsWith(type)) return true;
	            }
	        }

	        return false;
	    }

	    public String getDescription() {
	        return "." + prefix;
	    }
	}
	
	//***************constructor************
	public FileChooser(String _optionType,String directory,String extension){
		optionType=_optionType;
		fileChooser= new JFileChooser(modulDirectory+=directory);
		this.add(fileChooser);
		fileChooser.addActionListener(this);
		
		if(extension!=null){
			
			FileNameExtensionFilter filter = new FileNameExtensionFilter(getDescription(extension), extension);
			fileChooser.setFileFilter(filter);
		}
		
		
		if(optionType.equals(Options.Load)){
			
			fileChooser.setApproveButtonText(Options.Load);
		}else if(optionType.equals(Options.Save)){
			fileChooser.setApproveButtonText(Options.Save);
		}
	}
	
	
	
	//**************load save operation********
	/**
	 * Save file informations to text file
	 */
	public void saveFileDatas(BufferedWriter bw) throws IOException{
		bw.write("File\n");
		bw.write("Name\tValue\n");
		
		bw.write("FileName\t"+this.selectedFilename+"\n");
		bw.write("FilePath\t"+this.selectedFilePath+"\n");
		
		bw.write("EndFile\n");
	}
	
	/**
	 * Load file informations from text file
	 */
	public void loadFileDatas(BufferedReader br) throws IOException{
		
		String line = null;
		line = br.readLine();//reads header
		while (!(line = br.readLine()).equals("EndFile")) {
			String[] values = line.split("\\t");
			if(values[0].equals("FileName")){
				this.selectedFilename=values[1];
			}else if(values[0].equals("FilePath")){
				this.selectedFilePath=values[1];
			}
		}
		
		selectedFilePathAndName=selectedFilePath+"\\"+selectedFilename;
	}
	
	
	
	//************other functions*************
	public void appearWindow(){
		if(optionType.equals(Options.Load)){
			fileChooser.showOpenDialog(null);
		}else{
			fileChooser.showSaveDialog(null);
		}
		
	}
	
	public void setFilePath(String path){
		this.selectedFilePath=path;
	}
	
	
	
	//********getters****************
	public String getFilePath(){
		return selectedFilePath;
	}
	
	public String getFileName(){
		return selectedFilename;
	}
	
	public String getFilePathAndName(){
		return selectedFilePathAndName;
	}
	
	public boolean isFileSelected(){
		return fileSelected;
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
	    if (action.getActionCommand().equals(Options.CancelSelection))
	    {
	        setVisible(false);
	        dispose();
	    }
	    if (action.getActionCommand().equals(Options.ApproveSelection))
	    {
	    	fileSelected=true;
			selectedFilename=fileChooser.getSelectedFile().getName();
			selectedFilePath=modulDirectory;
			selectedFilePathAndName=modulDirectory+"\\"+selectedFilename;
			
			setVisible(false);
			dispose();
	    }
		
	}

	private String getDescription(String extension){
		String description="";
		if(extension.equals(LoadSaveModul.elementExtension)){
			description="Elements";
		}else if(extension.equals(LoadSaveModul.channelExtension)){
			description="Channels";
		}else {
			description="Protocols";
		}
		return description;
	}
	
}
