package ReusableModul;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class CommentPanel extends JPanel{
	private JTextArea textArea=new JTextArea();
	
	
	
	public CommentPanel(){
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Comment"));
		
		textArea.setFont(new Font("Serif", Font.ITALIC, 13));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true); 
		 
		JScrollPane textAreaScrollPane = new JScrollPane(textArea);
		textAreaScrollPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		textAreaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		this.add(textAreaScrollPane);
	}
	
	public CommentPanel(String text){
		this();
		textArea.setText(text);
	}
	
	//***********getters**************
	public String getText(){
		return textArea.getText();
	}
	
	
	
	//***********setters*************
	public void setText(String text){
		textArea.setText(text);
	}
	
	
	
	//*********other functions************
	public boolean isEmpty(){
		return textArea.getText().equals("");
	}

	
	
	//*********load save operations
	public void saveCommentPanel(BufferedWriter bw) throws IOException{
		bw.write("Comment\n");
		if(!isEmpty()){
			bw.write(textArea.getText()+"\n");
		}
		bw.write("EndComment\n");
	}
	
	public void loadCommentPanel(BufferedReader br) throws IOException{
		String text="";
		String line = null;
		while (!(line = br.readLine()).equals("EndComment")) {
			text+=line;
		}
		textArea.setText(text);
	}
}
