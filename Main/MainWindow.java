package Main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;

import temp.ChannelPropertiesWindow;
import temp.ProtocolPropertiesWindow;
import AbstractClasses.AbstractWaveTypeWindow;
import Chart.ChartsPanel;
import Converter.Converter;
import Converter.SendWindow;
import DataStructure.Channel;
import DataStructure.Protocol;
import Exception.WavedriveException;
import LoadSave.LoadSaveModul;
import NonParametricWave.NonParametricWindow;
import OptionWords.Options;
import PauseWave.PauseWindow;
import RampWave.RampWindow;
import SquareWave.SquareWindow;

/**
 * @author gemes.daniel
 * If a modify an object and then i try to read the same object before the modification than the reading will fail 
 *
 */

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	public static final String startingDirectory=System.getProperty("user.dir");
	private JPanel mainPanel;
	private ChartsPanel chartsPanel;
	private JPanel controlPanel;
	private JScrollPane scrollPanel;
	
	private JMenuBar menuBar;
	private JMenu mainMenu;
	
	private JComboBox<String> comboboxSelectWave;
	
	private String waveType;
	
	private Protocol protocol=new Protocol();
	
	
	//************inner classes*************************
	 public class ActionAddChannel extends AbstractAction {

		 @Override
	      public void actionPerformed(ActionEvent e) {
				Thread thread = new Thread() {
			        public void run() {
			        	ChannelPropertiesWindow channelPropertiesWindow=new ChannelPropertiesWindow(chartsPanel, protocol);
			        	
			        	MainWindow.this.setEnabled(false);
	        			
			            synchronized(channelPropertiesWindow.lock) {
		                    try {
		                    	channelPropertiesWindow.lock.wait();
		                    } catch (InterruptedException e) {
		                        e.printStackTrace();
		                    }
		                    MainWindow.this.setEnabled(true);
		                    requestFocus();
			           }
			        }
				};
				thread.start();
		 }
	   }
	 
	 public class ActionSend extends AbstractAction {

		 @Override
	      public void actionPerformed(ActionEvent e) {
			if(protocol.getChannels().size()!=0){
				Thread thread = new Thread() {
			        public void run() {
			        	SendWindow sendWindow=new SendWindow(protocol);;
			        	
			        	MainWindow.this.setEnabled(false);
	        			
			            synchronized(sendWindow.lock) {
		                    try {
		                    	sendWindow.lock.wait();
		                    } catch (InterruptedException e) {
		                        e.printStackTrace();
		                    }
		                    MainWindow.this.setEnabled(true);
		                    requestFocus();
			           }
			        }
				};
				thread.start();
				
				
			}else{
				JOptionPane.showMessageDialog (null, "Channals missing","Warning", JOptionPane.WARNING_MESSAGE);
			}		
	      }
	   }
	 
	 public class ActionSettings extends AbstractAction {

		 @Override
	      public void actionPerformed(ActionEvent e) {
			if(protocol.getChannels().size()!=0){
				Thread thread = new Thread() {
			        public void run() {
			        	ProtocolPropertiesWindow protocolPropertiesWindow=new ProtocolPropertiesWindow(protocol);
			        	
			        	MainWindow.this.setEnabled(false);
	        			
			            synchronized(protocolPropertiesWindow.lock) {
		                    try {
		                    	protocolPropertiesWindow.lock.wait();
		                    } catch (InterruptedException e) {
		                        e.printStackTrace();
		                    }
		                    MainWindow.this.setEnabled(true);
		                    requestFocus();
			           }
			        }
				};
				thread.start();
						
			}else{
				JOptionPane.showMessageDialog (null, "Channals missing","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionAppendElement extends AbstractAction {
		 String optionType=Options.Append;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 
			 Channel  selectedChannel=protocol.getSelectedChannel();
			 if(selectedChannel!=null){
				  waveType=(String)comboboxSelectWave.getSelectedItem();
					selectWaveformWindow(selectedChannel,optionType);
			 }else{
				 JOptionPane.showMessageDialog (null, "First select a channel","Warning", JOptionPane.WARNING_MESSAGE);
			 }
	      }
	   }
	 
	 public class ActionLoadAppendElement extends AbstractAction {
		 String optionType=Options.LoadAppendElement;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			if(selectedChannel!=null){
					new LoadSaveModul(chartsPanel,protocol, optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select a channel","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionInsertBefore extends AbstractAction {
		 String optionType=Options.InsertBefore;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			 if(selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
				 	waveType=(String)comboboxSelectWave.getSelectedItem();
					selectWaveformWindow(selectedChannel,optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select an element","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionLoadInsertElement extends AbstractAction {
		 String optionType=Options.LoadInsertElement;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			if(selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
					new LoadSaveModul(chartsPanel,protocol, optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select an element","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionSaveElement extends AbstractAction {
		 String optionType=Options.SaveElement;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			 if(selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
					new LoadSaveModul(chartsPanel, protocol, optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select an element","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionDeleteElement extends AbstractAction {

		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			 if(selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
				 int dialogButton = JOptionPane.YES_NO_OPTION;
				 int dialogResult= JOptionPane.showConfirmDialog (null, "Are you sure you want to delete an element?","Warning",dialogButton);
				 
	             if(dialogResult == JOptionPane.YES_OPTION){
	            	 protocol.deleteSelectedElement();
	             }
				
			}else{
				JOptionPane.showMessageDialog (null, "First select an element","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionModify extends AbstractAction {
		 String optionType=Options.Modify;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			 if(selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
					waveType=selectedChannel.getSelectedElement().getType();
					selectWaveformWindow(selectedChannel,optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select an element","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	
	 public class ActionLoadAppendChannel extends AbstractAction {
		 String optionType=Options.LoadAppendChannel;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			if(selectedChannel!=null){
					new LoadSaveModul(chartsPanel,protocol, optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select a channel","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionLoadInsertChannel extends AbstractAction {
		 String optionType=Options.LoadInsertChannel;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			 if(selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
					new LoadSaveModul(chartsPanel,protocol, optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select an element","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionSaveChannel extends AbstractAction {
		 String optionType=Options.SaveChannel;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 Channel selectedChannel=protocol.getSelectedChannel();
			 
			if(selectedChannel!=null){
				new LoadSaveModul(chartsPanel,protocol, optionType);
			}else{
				JOptionPane.showMessageDialog (null, "First select a channel","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionDeleteChannel extends AbstractAction {

		 @Override
	      public void actionPerformed(ActionEvent e) {
			 
			 Channel selectedChannel=protocol.getSelectedChannel();
			 if(selectedChannel!=null){
				 int dialogButton = JOptionPane.YES_NO_OPTION;
				 int dialogResult= JOptionPane.showConfirmDialog (null, "Are you sure you want to delete a channel?","Warning",dialogButton);
				 
	             if(dialogResult == JOptionPane.YES_OPTION){
						protocol.removeChannel();
						chartsPanel.removeChart();
	             }
			}else{
				JOptionPane.showMessageDialog (null, "First select a channel","Warning", JOptionPane.WARNING_MESSAGE);
			}
	      }
	   }
	 
	 public class ActionLoadProtocol extends AbstractAction {
		 String optionType=Options.LoadProtocol;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			new LoadSaveModul(chartsPanel,protocol, optionType);
	      }
	   }
	 
	 public class ActionSaveProtocol extends AbstractAction {
		 String optionType=Options.SaveProtocol;
		 
		 @Override
	      public void actionPerformed(ActionEvent e) {
			 new LoadSaveModul(chartsPanel,protocol, optionType);
	      }
	   }
	 
	 public class ActionDeleteProtocol extends AbstractAction {

		 @Override
	      public void actionPerformed(ActionEvent e) {
			 int dialogButton = JOptionPane.YES_NO_OPTION;
			 int dialogResult= JOptionPane.showConfirmDialog (null, "Are you sure you want to delete the protocol?","Warning",dialogButton);
			 
             if(dialogResult == JOptionPane.YES_OPTION){
     			protocol.removeProtocol();
    			chartsPanel.removeAllChart();
             }
	   }
	 }
	 
	 
	 
	//**************constructors*************
	/**
	 * Constructor of this class
	 */
	public MainWindow(){
		setWindowDesign();
		initializeComponents();
		initializeDirectories();
		
		pack();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	
	
	//******************* initialization functions*************
	/**
	 * Initialize starting directories
	 */
	private void initializeDirectories(){
		File dirSavedObjects = new File(startingDirectory+LoadSaveModul.directoryName);
		File dirFilesToArduino= new File(startingDirectory+Converter.directoryName);
		File dirNonParametricWaves = new File(startingDirectory+NonParametricWindow.directoryName);
		
		if (!dirSavedObjects.exists()) {
			dirSavedObjects.mkdirs();
		}
		
		if (!dirFilesToArduino.exists()) {
			dirFilesToArduino.mkdirs();
		}
		
		if (!dirNonParametricWaves.exists()) {
			dirNonParametricWaves.mkdirs();
		}
	}
	
	/**
	 * Set window design
	 */
	private void setWindowDesign(){
		setTitle("Wavedrive application");
		mainPanel=new JPanel();

		scrollPanel=new JScrollPane(mainPanel);
		
		setContentPane(scrollPanel);
		mainPanel.setLayout(new BorderLayout());
		
		
		//setLocationRelativeTo(null);
	}

	/**
	 * Set buttons properties
	 */
	private void setButton(JPanel panel,String name,Action action, int index) {
		JButton button=new JButton(name);
		button.addActionListener(action);
		panel.add(button,index);
		
		
	}
	
	/**
	 * Set menu item properties
	 */
	private void setMenu(JMenu submenu, String name, Action action,Integer mnemonic){
		
		JMenuItem menuItem=new JMenuItem(name);
		menuItem.setAccelerator(KeyStroke.getKeyStroke(
				mnemonic, ActionEvent.ALT_MASK));
		menuItem.addActionListener(action);
		submenu.add(menuItem);
		
	}
	
	/**
	 * Initialize Operation panel of protocol operation components
	 */
	private void InitializeProtocolOperationPanel(){
		JPanel protocolOperationPanel=new JPanel();
		
		protocolOperationPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Protocol operations"));
		
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setHgap(35);
		protocolOperationPanel.setLayout(gridLayout);
		
		JMenu submenuProtocolOperations=new JMenu("Protocol operations");
		
		//load
		Action actionLoadProtocol=new ActionLoadProtocol();
		setButton(protocolOperationPanel,"Load protocol",actionLoadProtocol,0);
		setMenu(submenuProtocolOperations,"Load protocol",actionLoadProtocol,KeyEvent.VK_Y);
		
		//save
		Action actionSaveProtocol=new ActionSaveProtocol();
		setButton(protocolOperationPanel,"Save protocol",actionSaveProtocol,1);
		setMenu(submenuProtocolOperations,"Save protocol",actionSaveProtocol,KeyEvent.VK_X);
		
		//delete
		Action actionDeleteProtocol=new ActionDeleteProtocol();
		setButton(protocolOperationPanel,"Delete protocol",actionDeleteProtocol,2);
		setMenu(submenuProtocolOperations,"Deleteprotocol",actionDeleteProtocol,KeyEvent.VK_V);
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=1;
		c.gridy=1;
		
		menuBar.add(submenuProtocolOperations);
		controlPanel.add(protocolOperationPanel,c);
	}
	
	/**
	 * Initialize Operation panel of channel operation components
	 */
	private void InitializeChannelOperationPanel(){
		JPanel channelOperationPanel=new JPanel();
		
		channelOperationPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Channel operations"));
		
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setHgap(35);
		
		channelOperationPanel.setLayout(gridLayout);
		
		JMenu submenuChannelOperations=new JMenu("Channel operations");
		
		//append
		Action actionLoadAppendChannel=new ActionLoadAppendChannel();
		setButton(channelOperationPanel,"Append loaded",actionLoadAppendChannel,0);
		setMenu(submenuChannelOperations,"Append loaded",actionLoadAppendChannel,KeyEvent.VK_P);
		
		//insert
		Action actionLoadInsertChannel=new ActionLoadInsertChannel();
		setButton(channelOperationPanel,"Insert before loaded",actionLoadInsertChannel,1);
		setMenu(submenuChannelOperations,"Insert before loaded",actionLoadInsertChannel,KeyEvent.VK_G);
		
		//save
		Action actionSaveChannel=new ActionSaveChannel();
		setButton(channelOperationPanel,"Save",actionSaveChannel,2);
		setMenu(submenuChannelOperations,"Save",actionSaveChannel,KeyEvent.VK_H);
		
		//delete
		Action actionDeleteChannel=new ActionDeleteChannel();
		setButton(channelOperationPanel,"Delete",actionDeleteChannel,3);
		setMenu(submenuChannelOperations,"Delete",actionDeleteChannel,KeyEvent.VK_J);
		
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=1;
		c.gridy=0;
		
		menuBar.add(submenuChannelOperations);
		controlPanel.add(channelOperationPanel,c);
	}
	
	/**
	 * Initialize Operation panel of element operation components
	 */
	private void InitializeElementOperationPanel(){
		JPanel elementOperationPanel=new JPanel();
		
		elementOperationPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Element operations"));
		
		GridLayout gridLayout = new GridLayout(4,2);
		gridLayout.setHgap(35);
		
		elementOperationPanel.setLayout(gridLayout);
		
		JMenu submenuElementOperations=new JMenu("Element operations");
		
		//select type
		String[] waveformChoices = { Options.Square,Options.Ramp,Options.NonParametric,Options.Pause};
		comboboxSelectWave= new JComboBox<String>(waveformChoices);
		elementOperationPanel.add(comboboxSelectWave,0);
		
		//append
		Action actionApppendElement=new ActionAppendElement();
		setButton(elementOperationPanel,"Append",actionApppendElement,1);
		setMenu(submenuElementOperations,"Append",actionApppendElement,KeyEvent.VK_R);
		
		//append loaded
		Action actionLoadAppendElement=new ActionLoadAppendElement();
		setButton(elementOperationPanel,"Append loaded",actionLoadAppendElement,2);
		setMenu(submenuElementOperations,"Append loaded",actionLoadAppendElement,KeyEvent.VK_T);
		
		//insert
		Action actionInsertBefore=new ActionInsertBefore();
		setButton(elementOperationPanel,"Insert before",actionInsertBefore,3);
		setMenu(submenuElementOperations,"Insert before",actionInsertBefore,KeyEvent.VK_I);
		
		//insert loaded
		Action actionLoadInsertElement=new ActionLoadInsertElement();
		setButton(elementOperationPanel,"Insert before loaded",actionLoadInsertElement,4);
		setMenu(submenuElementOperations,"Insert before loaded",actionLoadInsertElement,KeyEvent.VK_Z);
		
		//save
		Action actionSaveElement=new ActionSaveElement();
		setButton(elementOperationPanel,"Save",actionSaveElement,5);
		setMenu(submenuElementOperations,"Save",actionSaveElement,KeyEvent.VK_U);
		
		//delete
		Action actionDeleteElement=new ActionDeleteElement();
		setButton(elementOperationPanel,"Delete",actionDeleteElement,6);
		setMenu(submenuElementOperations,"Delete",actionDeleteElement,KeyEvent.VK_D);
		
		//modify
		Action actionModify=new ActionModify();
		setButton(elementOperationPanel,"Modify",actionModify,7);
		setMenu(submenuElementOperations,"Modify",actionModify,KeyEvent.VK_M);
		
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=1;
		
		menuBar.add(submenuElementOperations);
		controlPanel.add(elementOperationPanel,c);
	}
	
	/**
	 * Initialize Operation panel of basic operation components
	 */
	private void initializeBasicOperationPanel(){
		JPanel basicOperationPanel=new JPanel();
		
		basicOperationPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Basic operations"));
		
		GridLayout gridLayout = new GridLayout(2,2);
		gridLayout.setHgap(35);
		
	    basicOperationPanel.setLayout(gridLayout);
	    
	    JMenu submenuBasicOperations=new JMenu("Basic operations");
	    
		//add channel
		Action actionAddChannel=new ActionAddChannel();
		setButton(basicOperationPanel,"Add channel",actionAddChannel,0);
		setMenu(submenuBasicOperations,"Add channel",actionAddChannel,KeyEvent.VK_A);
		

		//send
		Action actionSend=new ActionSend();
		setButton(basicOperationPanel,"Send",actionSend,1);
		setMenu(submenuBasicOperations,"Send",actionSend,KeyEvent.VK_S);

		//settings
		Action actionSettings=new ActionSettings();
		setButton(basicOperationPanel,"Settings",actionSettings,2);
		setMenu(mainMenu,"Settings",actionSettings,KeyEvent.VK_O);
		
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=0;
		controlPanel.add(basicOperationPanel,c);
		
		menuBar.add(submenuBasicOperations);
	}
	
	private void initializeMenubar(){
		menuBar=new JMenuBar();
		mainMenu= new JMenu("Main");
		menuBar.add(mainMenu);
	}
	
	/**
	 * Call other initializing functions
	 */
	private void initializeComponents(){
		chartsPanel=new ChartsPanel(this);
		controlPanel=new JPanel(new GridBagLayout());
		
		
		initializeMenubar();
		initializeBasicOperationPanel();
		InitializeElementOperationPanel();
		InitializeChannelOperationPanel();
		InitializeProtocolOperationPanel();
		
		
		mainPanel.add(controlPanel,BorderLayout.NORTH);
		mainPanel.add(chartsPanel,BorderLayout.SOUTH);
		this.setJMenuBar(menuBar);
		
		//scrollPanel.setPreferredSize(new Dimension((int)Math.round(controlPanel.getPreferredSize().getWidth()),400));
	}

	

	//******************* other functions*************
	private void selectWaveformWindow(Channel selectedChannel,String optionType){
		
		
		Thread thread = new Thread() {
	        public void run() {
	        		AbstractWaveTypeWindow abstractWaveTypeWindow=null;
	        		try{
	        			System.out.println(waveType);
	        			switch(waveType){
	        			case Options.Square:
	        				abstractWaveTypeWindow= new SquareWindow(selectedChannel,optionType);
	        				break;
	        			case Options.NonParametric:
	        					abstractWaveTypeWindow= new NonParametricWindow(selectedChannel,optionType);
	        				break;
	        			case Options.Ramp:
	        					abstractWaveTypeWindow= new RampWindow(selectedChannel,optionType);
	        				break;
	        			case Options.Pause:
	        					abstractWaveTypeWindow= new PauseWindow(selectedChannel,optionType);
	        				break;
	        			}
	        		}catch (WavedriveException e) {
	        			System.out.println(e.getMessage());
	        			e.printStackTrace();
	        		}
	        			
        			setEnabled(false);
        			
		            synchronized(abstractWaveTypeWindow.lock) {
	                    try {
	                    	abstractWaveTypeWindow.lock.wait();
	                    } catch (InterruptedException e) {
	                        e.printStackTrace();
	                    }
	                    setEnabled(true);
	                    requestFocus();
		           }
	        			

	        }
	    };

	    thread.start();
		

	}
	
	
	
	/**
	 * Main method
	 */
	public static void main(String[] args) {
    	javax.swing.SwingUtilities.invokeLater(new Runnable() { 
    		public void run() { 
    				new MainWindow();
    		}});
	}

}























/**
 * Handles (Button) events 
 */
/*@Override
public void actionPerformed(ActionEvent e){
	try {
		Object obj=e.getSource();
		if(obj.getClass()==JButton.class){
			optionType=e.getActionCommand();
			selectedChannel=protocol.getSelectedChannel();
			waveType=(String)comboboxSelectWave.getSelectedItem();
			
			
			}else if(optionType.equals(Options.InsertBefore) && selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
				
				selectWaveformWindow();
				
			}else if(optionType.equals(Options.Modify) && selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
				
				waveType=selectedChannel.getSelectedElement().getType();
				selectWaveformWindow();
				
			}else if(optionType.equals(Options.SaveElement) && selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
				
				new LoadSaveModul(chartsPanel, protocol, optionType);
				
			}else if(optionType.equals(Options.LoadInsertElement) && selectedChannel!=null && selectedChannel.getSelectedElement()!=null){
				
				new LoadSaveModul(chartsPanel,protocol, optionType);
				
			}else if(optionType.equals(Options.LoadAppendElement) && selectedChannel!=null){
				
				new LoadSaveModul(chartsPanel,protocol, optionType);
				
			}else if(optionType.equals(Options.Settings) && protocol.getChannels().size()!=0){
				
				new ProtocolPropertiesWindow(protocol);
				
			}else{
				throw new WavedriveException("Button preconditions failed");
			}
		}else{
			throw new WavedriveException("Undefined Event");
		}
		pack();
	} catch (WavedriveException e1) {
		System.out.println("Error main frame event handling: "+e1.getMessage());
	}
	
}*/
