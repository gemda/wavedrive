package AbstractClasses;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;

import DataStructure.Channel;

import OptionWords.Options;

public abstract class AbstractWaveForm{
	public static Integer resolutionCorrection=10;	//Ms-> 1/10 Ms resolution
	
	protected LinkedList<Double> realValues=new LinkedList<Double>();	//data points in 1/10 Ms res
	
	protected String waveType=null;
	protected int startIndex=0;	//position in the chart
	protected boolean selectionState=false;
	
	protected String comment=null;
	protected Double baselinerelative=null;
	protected Double baselineabsolute=null;
	
	protected AbstractWaveTypeWindow abstractWaveTypeWindow;
	
	protected Channel channel;
	

	//**********cosntructors****************
	public AbstractWaveForm(Channel _channel, AbstractWaveTypeWindow _abstractWaveTypeWindow,String _type){
		channel=_channel;
		abstractWaveTypeWindow=_abstractWaveTypeWindow;
		waveType=_type;
		
		initializeVariables();

		if(waveType.equals(Options.NonParametric)){
			readFile();
		}
		
		calculateRealValues();
	}
	
	
	
	//************other functions*****************
	public abstract String toString();
	
	public String realvaluesOut(){
		String temp="";
		for(int i=0;i<realValues.size();i++){
			temp+=realValues.get(i)+"\t";
		}
		return temp;
	}
	
	public abstract void createOutput(BufferedWriter bw, Double min,Double max,Double res) throws IOException;
	
	public int convertToPWMValue(Double originalValue,Double min, Double max, Double res){
		Double range=Math.abs(max-min);
		//System.out.println(range);
		Double scale=res/range;
		//System.out.println(scale);
		if(originalValue<min){
			originalValue=min;
		}else if(originalValue>max){
			originalValue=max;
		}
		Double shiftedValue=originalValue+Math.abs(min);
		//System.out.println(shiftedValue);
		//System.out.println(shiftedValue*scale);
		return (int)Math.round(shiftedValue*scale);
	}
	

	
	protected void readFile(){}
	
	public void resetBaseline(){
		abstractWaveTypeWindow.getOptionPanel().resetBaseline();
	}
	
	protected abstract void initializeOtherVariables();
	
	/**
	 * Initialize class members 
	 */
	protected void initializeVariables(){
		
		initializeOtherVariables();
		
		this.baselineabsolute=abstractWaveTypeWindow.getBaseabs();
		this.baselinerelative=abstractWaveTypeWindow.getBaserel();
		
		if(baselinerelative!=null){
			String op=abstractWaveTypeWindow.getOptionType();
			if(op.equals(Options.Append) || op.equals(Options.LoadAppendElement) ||op.equals(Options.LoadAppendChannel)){
				baselineabsolute=channel.getAppendAligment()+baselinerelative;
			}else{
				
				baselineabsolute=channel.getInsertAligment()+baselinerelative;
			}
		}
	}
	
	
	
	//*************getters*****************************
	public AbstractWaveForm getWavefrom() {
		return this;
	}

	public LinkedList<Double> getRealValues() {
		return this.realValues;
	}
	
	public int getStartIndex() {
		return this.startIndex;
	}
	
	public int getEndIndex() {
		return this.startIndex+realValues.size();
	}
	
	public String getType() {
		return this.waveType;
	}
	
	public boolean isSelected() {
		return selectionState;
		
	}
	
	public Double getBaselineAbsolute(){
		return baselineabsolute;
	}
	
	public abstract int getCodedLength();
	
	public abstract AbstractWaveTypeWindow getTypeWindow();
	
	
	
	//************setters**********************
	public void setRangeValues(int _startIndex) {
		startIndex=_startIndex;
		
	}

	public void setSelection(boolean state) {
		selectionState=state;
	}

	public void refreshRealValues(Double baselineOfPrevious){
		realValues.removeAll(realValues);
		
		if(baselinerelative!=null){
			baselineabsolute=baselineOfPrevious+baselinerelative;
		}
			
		calculateRealValues();
	}
	
	
	
	public abstract void  calculateRealValues();
	
	public static void setResolution(Integer res){
		resolutionCorrection=res;
	}
	
}
