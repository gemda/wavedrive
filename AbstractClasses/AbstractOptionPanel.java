package AbstractClasses;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import Exception.WavedriveException;
import SquareWave.SquareWindow;

@SuppressWarnings("serial")
public abstract class AbstractOptionPanel extends JPanel{
	//baseline
	protected JLabel baselineLabelName=new JLabel("Baseline (rel/abs)");
	protected JRadioButton baselineChoice1=new JRadioButton();
	protected SpinnerModel baselineRelSpinnerModell=new SpinnerNumberModel(0,-1000.0,1000,1);
	protected JSpinner baselineRelSpinner=new JSpinner(baselineRelSpinnerModell);
	protected JLabel baselineRelLabelUnit=new JLabel("mV");
	protected JRadioButton baselineChoice2=new JRadioButton();
	protected SpinnerModel baselineAbsSpinnerModell=new SpinnerNumberModel(0.0,-1000.0,1000.0,0.1);
	protected JSpinner baselineAbsSpinner=new JSpinner(baselineAbsSpinnerModell);
	protected JLabel baselineAbsLabelUnit=new JLabel("mV");
	protected ButtonGroup baselineButtonGroup=new ButtonGroup();
	
	
	//***************constructors*****************
	public AbstractOptionPanel(){}
	
	
	
	//***************other functions***************
	protected void copyBaselineOptions(AbstractOptionPanel otherAbstractOptionPanel){
		this.baselineAbsSpinner.setValue(otherAbstractOptionPanel.baselineAbsSpinner.getValue());
		this.baselineAbsSpinner.setEnabled(otherAbstractOptionPanel.baselineAbsSpinner.isEnabled());
		this.baselineRelSpinner.setValue(otherAbstractOptionPanel.baselineRelSpinner.getValue());
		this.baselineRelSpinner.setEnabled(otherAbstractOptionPanel.baselineRelSpinner.isEnabled());
		this.baselineChoice1=otherAbstractOptionPanel.baselineChoice1;
		this.baselineChoice2=otherAbstractOptionPanel.baselineChoice2;
	}
	
	public void resetBaseline() {
		baselineChoice1.setSelected(true);
		baselineRelSpinner.setValue(0.0);
		baselineChoice2.setSelected(false);
		baselineAbsSpinner.setValue(0.0);
	}
	
	//**************save load operaions************
	public void saveOptionPanel(BufferedWriter bw) throws IOException{
		bw.write("Options\n");
		bw.write("Name\tValue\n");
		
		saveOtherOptions(bw);
		
		bw.write("BaselineChoice1\t"+this.baselineChoice1.isSelected()+"\n");
		bw.write("BaselineReal\t"+this.baselineRelSpinner.getValue()+"\n");
		bw.write("BaselineChoice2\t"+this.baselineChoice2.isSelected()+"\n");
		bw.write("BaselineAbs\t"+this.baselineAbsSpinner.getValue()+"\n");
		
		bw.write("EndOptions\n");
	}
	
	protected abstract void saveOtherOptions(BufferedWriter bw) throws IOException;
	
	public abstract void loadOptionPanel(BufferedReader br) throws IOException, WavedriveException;
	
	
	//**************setters********************
	protected void setBaselineComponents(){
		this.add(baselineLabelName);
		this.add(baselineChoice1);
		baselineChoice1.addActionListener(new SquareWindow.RadioButtonActionListener1(baselineChoice1,baselineRelSpinner,baselineAbsSpinner));
		this.add(baselineRelSpinner);
		this.add(baselineRelLabelUnit);
		this.add(baselineChoice2);
		baselineChoice2.addActionListener(new SquareWindow.RadioButtonActionListener1(baselineChoice2,baselineAbsSpinner,baselineRelSpinner));
		this.add(baselineAbsSpinner);
		this.add(baselineAbsLabelUnit);
		baselineButtonGroup.add(baselineChoice1);
		baselineButtonGroup.add(baselineChoice2);
		
		baselineChoice1.setSelected(true);
		baselineAbsSpinner.setEnabled(false);
	}
	
	public void setRelBaseline(Double relativeValDiff){
		this.baselineRelSpinner.setValue(relativeValDiff);
	}
	
	protected void setBaselineSettingsAfterLoading(){
		if(this.baselineChoice1.isSelected()){
			this.baselineRelSpinner.setEnabled(true);
			this.baselineAbsSpinner.setEnabled(false);
		}else{
			this.baselineRelSpinner.setEnabled(false);
			this.baselineAbsSpinner.setEnabled(true);
		}
	}

	
	
	
	//*************getters********************
	public JRadioButton getBaselineChoice1() {
		return baselineChoice1;
	}

	public JSpinner getBaselineRelSpinner() {
		return baselineRelSpinner;
	}

	public JSpinner getBaselineAbsSpinner() {
		return baselineAbsSpinner;
	}
}
