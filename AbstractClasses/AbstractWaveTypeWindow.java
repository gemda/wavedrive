package AbstractClasses;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;

import DataStructure.Channel;
import Exception.WavedriveException;
import Main.MainWindow;
import NonParametricWave.NonParametricOptionPanel;
import NonParametricWave.NonParametricWindow;
import OptionWords.Options;
import PauseWave.PauseOptionPanel;
import PauseWave.PauseWindow;
import RampWave.RampOptionPanel;
import RampWave.RampWindow;
import ReusableModul.CommentPanel;
import SquareWave.SquareOptionPanel;
import SquareWave.SquareWindow;

@SuppressWarnings("serial")
public abstract class AbstractWaveTypeWindow  extends JFrame implements ActionListener{
	protected MainWindow mainWindow=null;
	protected Channel channel=null;
	protected String optionType=null;
	protected String windowType=null;
	protected String comment=null;
	
	protected JPanel mainPanel;
	protected AbstractOptionPanel abstractOptionPanel;
	protected CommentPanel commentPanel;
	protected JPanel buttonPanel;
	
	public Object lock= new Object();
	
	protected Double baserel=null,baseabs=null;
	
	/**
	 * This class handles the RadioButton operations
	 * From two options enable the selected one's field and disable the other
	 * ButtonGroup handles that only one RadioButton can be selected
	 */
	 public static class RadioButtonActionListener1 implements Serializable, ActionListener{
		private static final long serialVersionUID = 1L;
		JRadioButton radio=null;
		JSpinner spinner1=null;
		JSpinner spinner2=null;
		public RadioButtonActionListener1(JRadioButton _radio,JSpinner _spinner1,JSpinner _spinner2){
			radio=_radio;
			spinner1=_spinner1;
			spinner2=_spinner2;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
				spinner1.setValue(0);
				spinner1.setEnabled(true);
				spinner2.setValue(0);
				spinner2.setEnabled(false);
		}
	}
	
	/**
	 * This class handles the RadioButton operations
	 * From two options enable the selected one's field and disable the other
	 * ButtonGroup handles that only one RadioButton can be selected
	 */
	public static class RadioButtonActionListener2 implements Serializable, ActionListener{
		private static final long serialVersionUID = 1L;
		JRadioButton radio=null;
		JSpinner spinner1=null;
		JSpinner spinner2=null;
		JSpinner spinner3=null;
		JSpinner spinner4=null;
		boolean type;
		public RadioButtonActionListener2(JRadioButton _radio,JSpinner _spinner1,JSpinner _spinner2,JSpinner _spinner3,JSpinner _spinner4,boolean _type){
			radio=_radio;
			spinner1=_spinner1;
			spinner2=_spinner2;
			spinner3=_spinner3;
			spinner4=_spinner4;
			type=_type;
		}
		
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(type){
				spinner1.setEnabled(true);
				spinner2.setEnabled(true);
				spinner3.setEnabled(false);
				spinner4.setEnabled(false);
			}else{
				spinner1.setEnabled(false);
				spinner2.setEnabled(false);
				spinner3.setEnabled(true);
				spinner4.setEnabled(true);
			}
			spinner4.setValue(0);
			spinner3.setValue(0);
			spinner2.setValue(0);
			spinner1.setValue(0);

		}
	}
	 
	
	//***************constructor*****************
	/**
	 * First constructor. Creates new AbstractWaveTypeWindow object or perform modify operation. Also shows AbstractWaveTypeWindow.
	 */
	public AbstractWaveTypeWindow(Channel _channel, String _optionType,String _windowType){
		channel=_channel;
		optionType=_optionType;
		windowType=_windowType;
		
		createPanels();

		mainPanel.add(abstractOptionPanel);
		mainPanel.add(commentPanel);
		mainPanel.add(buttonPanel);
		
		this.addWindowListener(new WindowAdapter() {
	         @Override
	         public void windowClosing(WindowEvent evt) {
	             synchronized (lock) {
	                 setVisible(false);
	                 lock.notify();
	             }
	         }
	      });
		
		setTitle(getTitleForWindow());
		setVisible(true);
		pack();
	};
	
	/**
	 * Second Constructor. loads AbstractWaveTypeWindow object from file
	 */
	public AbstractWaveTypeWindow(Channel _channel,String _optionType, String _windowType, BufferedReader br){
		channel=_channel;
		optionType=_optionType;
		windowType=_windowType;
		
		try {
			createPanels();
			loadWindow(br);
			
		} catch (IOException | WavedriveException e) {
			
			e.printStackTrace();
		}
	}
	
	public AbstractWaveTypeWindow(){}
	

	
	//****************other functions**************
	/**
	 * Convert Spinners object type return value to a double value
	 * @input: Object from spinner
	 * @output: Double value 
	 */
	protected Double objectToDouble(Object object){
		return Double.parseDouble(object.toString());
	}
	

	/**
	 * Set relative baseline to a given value. Neede after modify, insert or delete operation
	 */
	protected void adjustBaseline(Double relativeValDiff){
		if(baserel!=null){
			abstractOptionPanel.setRelBaseline(relativeValDiff);
		}
	}
	
	/**
	 * Create panels
	 */
	protected void createPanels(){
		mainPanel=new JPanel();
		setContentPane(mainPanel);
		
		this.setLayout(new GridLayout(3,1));
		
		try {
			createOptionPanel();
			createCommentPanel();
			createButtonPanel();
			
		} catch (WavedriveException e) {
			System.out.println("Problem with initializing panels");
			e.printStackTrace();
		}
	}
	
	/**
	 * Organize Option panel
	 */
	protected void createOptionPanel() throws WavedriveException{

		if(optionType.equals(Options.Modify)){
			if(channel.getSelectedElement().getTypeWindow() instanceof SquareWindow){
				SquareWindow otherSquareWindow=(SquareWindow)channel.getSelectedElement().getTypeWindow();
				abstractOptionPanel=new SquareOptionPanel(otherSquareWindow.getOptionPanel());
			}else if(channel.getSelectedElement().getTypeWindow() instanceof NonParametricWindow){
				NonParametricWindow otherNonParametricWindow=(NonParametricWindow)channel.getSelectedElement().getTypeWindow();
				abstractOptionPanel=new NonParametricOptionPanel(otherNonParametricWindow.getOptionPanel());
			}else if(channel.getSelectedElement().getTypeWindow() instanceof RampWindow){
				RampWindow otherRampWindow=(RampWindow)channel.getSelectedElement().getTypeWindow();
				abstractOptionPanel=new RampOptionPanel(otherRampWindow.getOptionPanel());
			}else if(channel.getSelectedElement().getTypeWindow() instanceof PauseWindow){
				PauseWindow otherPauseWindow=(PauseWindow)channel.getSelectedElement().getTypeWindow();
				abstractOptionPanel=new PauseOptionPanel(otherPauseWindow.getOptionPanel());
			}else{
				throw new WavedriveException("Instanceof problem in SquareWindow");
			}
		}else{
			if(windowType.equals(Options.SquareWindow)){
				abstractOptionPanel=new SquareOptionPanel();
			}else if(windowType.equals(Options.NonParametricWindow)){
				abstractOptionPanel=new NonParametricOptionPanel();
			}else if(windowType.equals(Options.RampWindow)){
				abstractOptionPanel=new RampOptionPanel();
			}else if(windowType.equals(Options.PauseWindow)){
				abstractOptionPanel=new PauseOptionPanel();
			}else{
				throw new WavedriveException("Not recognizable Option word");
			}
		}
		
		abstractOptionPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
		        .createLineBorder(Color.BLUE), "Options"));
		
		
	}
	
	/**
	 * Organize comment panel
	 */
	protected void createCommentPanel() throws WavedriveException{
		if(optionType.equals(Options.Modify)){
			AbstractWaveTypeWindow otherAbstractWaveTypeWindow=channel.getSelectedElement().getTypeWindow();
			commentPanel=new CommentPanel(otherAbstractWaveTypeWindow.getComment());
		}else{
			commentPanel=new CommentPanel();
		}
	}
	
	/**
	 * Organize panel for generate button
	 */
	protected void createButtonPanel(){
		buttonPanel=new JPanel();
		JButton generateButton=new JButton("Generate");
		
		generateButton.addActionListener(this);
		buttonPanel.add(generateButton);
	}
	
	protected abstract void checkAndGetValues()throws WavedriveException;
	
	protected void getBaselineAndComment(){
		if(abstractOptionPanel.getBaselineChoice1().isSelected()){
			baserel=objectToDouble(abstractOptionPanel.getBaselineRelSpinner().getValue());
		}else{
			baseabs=objectToDouble(abstractOptionPanel.getBaselineAbsSpinner().getValue());
		}
		
		if(!commentPanel.isEmpty()){
			comment=commentPanel.getText();
		}
	}
	
	
	
	//*************save load operations**************
	public abstract void saveWindow(BufferedWriter bw) throws IOException;
	
	protected abstract void loadWindow(BufferedReader br) throws IOException, WavedriveException;
	
	
	
	//**************getters*************
 	public Channel getContainingChannel(){
		return channel;
	}

	public String getOptionType(){
		return this.optionType;
	}
	
	public String getTypeOfWindow(){
		return this.windowType;
	}

	public String getComment(){
		return comment;
	}

	public Double getBaserel() {
		return baserel;
	}

	public Double getBaseabs() {
		return baseabs;
	}
	
	public abstract AbstractOptionPanel getOptionPanel();
	
	public String getTitleForWindow(){
		String title="";
		
		String waveType="";
		switch(windowType){
		case(Options.SquareWindow): waveType=Options.Square;
			break;
		case(Options.RampWindow): waveType=Options.Ramp;
			break;
		case(Options.NonParametricWindow): waveType=Options.NonParametric;
			break;
		case(Options.PauseWindow): waveType=Options.Pause;
			break;
		}
		
		
		switch(optionType){
		case(Options.Append): title="Append "+waveType+" element";
			break;
		case(Options.InsertBefore): title="Insert before "+waveType+" element";
			break;
		case(Options.Modify): title="Modify "+waveType+" element";
			break;
		/*case(Options.LoadInsertElement): title="Insert before loaded "+waveType+" element";
			break;
		case(Options.LoadAppendElement): title="Append loaded "+waveType+" element";
			break;*/
		}
		
		return title;
	}
}
