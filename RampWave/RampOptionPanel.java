package RampWave;

import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import AbstractClasses.AbstractOptionPanel;
import Exception.WavedriveException;

@SuppressWarnings("serial")
public class RampOptionPanel extends AbstractOptionPanel{
	//amplitude
	private JLabel amplLabelName=new JLabel("Amplitude");
	private SpinnerModel amplSpinnerModell=new SpinnerNumberModel(0.0,-10000.0,10000.0,0.1);
	private JSpinner amplSpinner=new JSpinner(amplSpinnerModell);
	private JLabel amplLabelUnit=new JLabel("mV");
	private JLabel ampldurationLabelName=new JLabel("Duration");
	private SpinnerModel durationSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner ampldurationSpinner=new JSpinner(durationSpinnerModell);
	private JLabel ampldurationLabelUnit=new JLabel("ms");
	private JRadioButton ampldurChoice=new JRadioButton();
	
	//slope
	private JLabel slopeLabelName=new JLabel("Slope");
	private SpinnerModel slopeSpinnerModell=new SpinnerNumberModel(0.0,-10000.0,10000.0,0.1);
	private JSpinner slopeSpinner=new JSpinner(slopeSpinnerModell);
	private JLabel slopedurationLabelName=new JLabel("Duration");
	private SpinnerModel slopedurationSpinnerModell=new SpinnerNumberModel(0.0,0.0,10000.0,0.1);
	private JSpinner slopedurationSpinner=new JSpinner(slopedurationSpinnerModell);
	private JLabel slopedurationLabelUnit=new JLabel("ms");
	private JRadioButton slopeChoice=new JRadioButton();

	
	private ButtonGroup optionButtonGroup=new ButtonGroup();
	
	
	//***************constructors*************
	 /**
	  * First constructor. Creates new RampOptionPanel object
	  */
	public RampOptionPanel(){
		startingFormation();
		addComponentsAndListeners();
	}
	
	/**
	 * Second constructor for modify operation
	 * Set values and visibility of the original element objects  to appear a window with the same option
	 */
	public RampOptionPanel(RampOptionPanel otherRampOptionPanel){
		this.amplSpinner.setValue(otherRampOptionPanel.getAmplSpinner().getValue());
		this.amplSpinner.setEnabled(otherRampOptionPanel.getAmplSpinner().isEnabled());
		this.ampldurationSpinner.setValue(otherRampOptionPanel.getAmpldurationSpinner().getValue());
		this.ampldurationSpinner.setEnabled(otherRampOptionPanel.getAmpldurationSpinner().isEnabled());
		
		this.slopeSpinner.setValue(otherRampOptionPanel.getSlopeSpinner().getValue());
		this.slopeSpinner.setEnabled(otherRampOptionPanel.getSlopeSpinner().isEnabled());
		this.slopedurationSpinner.setValue(otherRampOptionPanel.getSlopedurationSpinner().getValue());
		this.slopedurationSpinner.setEnabled(otherRampOptionPanel.getSlopedurationSpinner().isEnabled());
		
		this.ampldurChoice=otherRampOptionPanel.getAmpldurChoice();
		this.slopeChoice=otherRampOptionPanel.getSlopeChoice();
		
		copyBaselineOptions(otherRampOptionPanel);
		addComponentsAndListeners();
	}
	
	
	
	//*************save load operations***********************
	/**
	 * Save options to text file
	 */
	public void saveOtherOptions(BufferedWriter bw) throws IOException{
		bw.write("AmplitudeDurationChoice\t"+this.ampldurChoice.isSelected()+"\n");
		bw.write("Amplitude\t"+this.amplSpinner.getValue()+"\n");
		bw.write("AmplitudeDuration\t"+this.ampldurationSpinner.getValue()+"\n");
		
		bw.write("SlopeChoice\t"+this.slopeChoice.isSelected()+"\n");
		bw.write("SlopeSpinner\t"+this.slopeSpinner.getValue()+"\n");
		bw.write("SlopeDuration\t"+this.slopedurationSpinner.getValue()+"\n");
	}
	
	/**
	 * Load options from text file
	 */
	public void loadOptionPanel(BufferedReader br) throws IOException, WavedriveException{
		
		String line = null;
		line = br.readLine();//reads header
		while (!(line = br.readLine()).equals("EndOptions")) {
			String[] values = line.split("\\t");
			if(values[0].equals("Amplitude")){
				this.amplSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("AmplitudeDurationChoice")){
				this.ampldurChoice.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("AmplitudeDuration")){
				this.ampldurationSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("SlopeChoice")){
				this.slopeChoice.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("SlopeSpinner")){
				this.slopeSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("SlopeDuration")){
				this.slopedurationSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("BaselineChoice1")){
				this.baselineChoice1.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineReal")){
				this.baselineRelSpinner.setValue(Double.parseDouble(values[1]));
			}else if(values[0].equals("BaselineChoice2")){
				this.baselineChoice2.setSelected(Boolean.parseBoolean(values[1]));
			}else if(values[0].equals("BaselineAbs")){
				this.baselineAbsSpinner.setValue(Double.parseDouble(values[1]));
			}else{
				throw new WavedriveException("Read unknow variable from file: "+values[0]);
			}
		}
		
		if(ampldurChoice.isSelected()){
			this.ampldurationSpinner.setEnabled(true);
			this.amplSpinner.setEnabled(true);
			this.slopedurationSpinner.setEnabled(false);
			this.slopeSpinner.setEnabled(false);
		}else{
			this.ampldurationSpinner.setEnabled(false);
			this.amplSpinner.setEnabled(false);
			this.slopedurationSpinner.setEnabled(true);
			this.slopeSpinner.setEnabled(true);
		}
		
		setBaselineSettingsAfterLoading();
	}
	
	
	
	//*************other functions***************
	 /**
	  *  Set starting formation of RadioButtons and Spinners
	  */
	private void startingFormation(){
		ampldurChoice.setSelected(true);
		amplSpinner.setEnabled(true);
		ampldurationSpinner.setEnabled(true);
		
		slopeChoice.setSelected(false);
		slopeSpinner.setEnabled(false);
		slopedurationSpinner.setEnabled(false);
	}

	/**
	 * Add components to Panel, set Layout, add ActionListeners
	 */
	private void addComponentsAndListeners(){
		GridLayout gridLayout=new GridLayout(3,7);
		setLayout(gridLayout);
		gridLayout.setVgap(10);
		
		//amplitude + dur
		this.add(ampldurChoice);
		this.add(amplLabelName);
		this.add(amplSpinner);
		this.add(amplLabelUnit);
		this.add(ampldurationLabelName);
		this.add(ampldurationSpinner);
		this.add(ampldurationLabelUnit);
		ampldurChoice.addActionListener(new RampWindow.RadioButtonActionListener2(ampldurChoice,amplSpinner,ampldurationSpinner,slopeSpinner,slopedurationSpinner,true));
		optionButtonGroup.add(ampldurChoice);
		
		//slope + dur
		this.add(slopeChoice);
		this.add(slopeLabelName);
		this.add(slopeSpinner);
		this.add(new JLabel());
		this.add(slopedurationLabelName);
		this.add(slopedurationSpinner);
		this.add(slopedurationLabelUnit);
		slopeChoice.addActionListener(new RampWindow.RadioButtonActionListener2(slopeChoice,amplSpinner,ampldurationSpinner,slopeSpinner,slopedurationSpinner,false));
		optionButtonGroup.add(slopeChoice);
		
		//baseline
		setBaselineComponents();
	}
	
	
	
	//**********getters***********
	public JSpinner getAmplSpinner() {
		return amplSpinner;
	}

	public JSpinner getAmpldurationSpinner() {
		return ampldurationSpinner;
	}

	public JRadioButton getAmpldurChoice() {
		return ampldurChoice;
	}

	public JSpinner getSlopeSpinner() {
		return slopeSpinner;
	}

	public JSpinner getSlopedurationSpinner() {
		return slopedurationSpinner;
	}

	public JRadioButton getSlopeChoice() {
		return slopeChoice;
	}

}
  