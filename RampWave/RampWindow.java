package RampWave;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import AbstractClasses.AbstractWaveTypeWindow;
import DataStructure.Channel;
import Exception.WavedriveException;
import OptionWords.Options;

@SuppressWarnings("serial")
public class RampWindow extends AbstractWaveTypeWindow{
	private RampOptionPanel optionPanel=(RampOptionPanel)abstractOptionPanel;
	
	Double ampl=null,ampldur=null,slope=null, slopedur=null;
	
	
	//*************constructors************
	/**
	 * First constructor. Creates new TriangleWindow object or perform modify operation. Also shows RampWindow.
	 * @throws WavedriveException 
	 */
	public RampWindow(Channel _channel, String _optionType) throws WavedriveException{
		super(_channel, _optionType,Options.RampWindow);
	}
	
	
	/**
	 * Second Constructor. Loads TriangleWindow object from file
	 */
	public RampWindow(Channel _channel,String _optionType, BufferedReader br) throws WavedriveException{
		super(_channel, _optionType,Options.RampWindow,br);
		checkAndGetValues();
	}
	
	
	
	//**************other functions***********************
	/**
	 * Read values from active Spinners and check their validity
	 */
	protected void checkAndGetValues() throws WavedriveException{
		if(optionPanel.getAmpldurChoice().isSelected()){
			ampl=objectToDouble(optionPanel.getAmplSpinner().getValue());
			if(ampl==0)
				throw new WavedriveException("Wrong amplitude value!");
			
			ampldur=objectToDouble(optionPanel.getAmpldurationSpinner().getValue());
			if(ampldur==0)
				throw new WavedriveException("Wrong amplitude duration value!");
		}else{
			slope=objectToDouble(optionPanel.getSlopeSpinner().getValue());
			if(slope==0)
				throw new WavedriveException("Wrong slope value!");
			
			slopedur=objectToDouble(optionPanel.getSlopedurationSpinner().getValue());
			if(slopedur==0)
				throw new WavedriveException("Wrong slope duration value!");
		}
		
		getBaselineAndComment();
	}
	
	/**
	 * Save  TriangleWindow with options
	 */
	public void saveWindow(BufferedWriter bw) throws IOException{
		bw.write("Ramp\n");
		commentPanel.saveCommentPanel(bw);
		optionPanel.saveOptionPanel(bw);
		bw.write("EndRamp\n");
		
	}

	protected void loadWindow(BufferedReader br) throws IOException,WavedriveException{

		String line = null;
		while (!(line = br.readLine()).equals("EndRamp")) {
			if(line.equals("Comment")){
				commentPanel.loadCommentPanel(br);
			}else if(line.equals("Options")){
				((RampOptionPanel)abstractOptionPanel).loadOptionPanel(br);
			}else{
				throw new WavedriveException("Read unknown data group identifier from textfile: "+line);
			}
		}
	}
	
	/**
	 * Set relative baseline to a given value. Neede after modify, insert or delete operation
	 */
	public void adjustBaseline(Double relativeValDiff){
		if(getBaserel()!=null){
			optionPanel.setRelBaseline(relativeValDiff);
		}
	}
	
	
	
	//************getters************
	public Double getAmpl() {
		return ampl;
	}

	public Double getAmpldur() {
		return ampldur;
	}

	public Double getSlopedur() {
		return slopedur;
	}

	public Double getSlope() {
		return slope;
	}
	
	public RampOptionPanel getOptionPanel() {
		return this.optionPanel;
	}
	
	
	
	//********action event handler*************
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			Object obj=e.getSource();
			if(obj.getClass()==JButton.class){
				
				checkAndGetValues();
				channel.add(this);
				
				dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
			}
		}catch(WavedriveException e1) {
			System.out.println("Error in RampWindow frame event handling: "+e1.getMessage());
			JOptionPane.showMessageDialog (null, e1.getMessage() ,"Warning", JOptionPane.WARNING_MESSAGE);
		}
	}

}
