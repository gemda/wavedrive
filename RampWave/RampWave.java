package RampWave;

import java.io.BufferedWriter;
import java.io.IOException;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Channel;
import OptionWords.Options;

public class RampWave extends AbstractWaveForm{
	private static final int codeIdentifier=600000;
	
	private Double amplitude;
	private Double amplduration;
	private Double slope;
	private Double slopeduration;
	
	
	//*****************constructors********
	/**
	 * Creates new RampWave element, initialize variables
	 */
	public RampWave(Channel _channel, RampWindow _rampWindow) {
		super(_channel,_rampWindow,Options.Ramp);
		
		//System.out.println(this.toString());
	}
	
	
	
	//******other functions*************
	/**
	 * Initialize other class members 
	 */
	protected void initializeOtherVariables(){
		
		this.amplitude=((RampWindow)abstractWaveTypeWindow).getAmpl();
		this.amplduration=((RampWindow)abstractWaveTypeWindow).getAmpldur();
		this.slope=((RampWindow)abstractWaveTypeWindow).getSlope();
		this.slopeduration=((RampWindow)abstractWaveTypeWindow).getSlopedur();
		this.baselineabsolute=((RampWindow)abstractWaveTypeWindow).getBaseabs();
		this.baselinerelative=((RampWindow)abstractWaveTypeWindow).getBaserel();
		
		if(slope==null){
			slope=amplitude/amplduration;
			slopeduration=amplduration;
		}
		
		if(amplitude==null){
			amplitude=slope*slopeduration;
			amplduration=slopeduration;
		}
	}
	
	/**
	 * Calculates real values from the given parameters
	 * the point is that transform equivalent parameters into those which are most fitting to programming
	 */
	public void calculateRealValues(){
		
		Double step=slope/resolutionCorrection;
		Double val=0.0;
		//System.out.println(baselineabsolute);
		for(int i=0;i<slopeduration*resolutionCorrection;i++){
			this.realValues.add(baselineabsolute+val);
			val+=step;
		}
		
		//correction because of the rounding failures
		realValues.removeLast();
		realValues.add(this.amplitude+this.baselineabsolute);

		//System.out.println("vege");
	}
	
	/**
	 * ToString method for parameters and realValues
	 */
	@Override
	public String toString() {
		return "TriangleWave [amplitude="+amplitude+", amplduration="+ amplduration+", slope="+slope+
				", slopeduration="+slopeduration+ ", baseline abs="+baselineabsolute+
				", baseline relative="+baselinerelative+", comment="+comment+"]\n"+realvaluesOut();
	}
	
	/**
	 * Creates coded output for Arduino
	 */
	public void createOutput(BufferedWriter bw,Double min, Double max,Double res) throws IOException {
		bw.write(codeIdentifier+"\n");
		bw.write(convertToPWMValue(this.baselineabsolute, min, max,res)+"\n");
		bw.write((int)Math.round(this.amplduration)+"\n");
		bw.write(convertToPWMValue(this.baselineabsolute+this.amplitude, min, max,res)+"\n");
	};
	
	
	
	//*************getters*******************
	@Override
	public RampWindow getTypeWindow() {
		return ((RampWindow)abstractWaveTypeWindow);
	}
	
	public int getCodedLength(){
		return 3+1;
	}
}
