package Converter;

/*This is how the output sholud look like
 * # data points
 * # channels
 * --------------SQUARE----------
 * # data points on the channel
 * identifier of the channel
 * code word
 * duration
 * amplitude
 * --------------TRIANGLE-------
 * # data points on the channel
 * identifier of the channel
 * code word
 * baseline
 * duration
 * new baseline
 * --------------NONPARAM-------
 * # data points on the channel
 * identifier of the channel
 * data points
 */


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import AbstractClasses.AbstractWaveForm;
import DataStructure.Channel;
import DataStructure.Protocol;
import Main.MainWindow;

public class Converter {
	public static final String defaultFileName="\\StimulationPatternToArduino";
	public static final String directoryName="\\_Datas\\FilesToArduino";
	
	private Protocol protocol;
	private LinkedList<Channel> channels=new LinkedList<>();
	private BufferedWriter bw=null;
	
	int numberOfAllDatas=0;
	private Integer arduinoPort;
	
	public Converter(Protocol _protocol,String filePath, Integer _arduinoPort){
		protocol=_protocol;
		channels=protocol.getChannels();
		arduinoPort=_arduinoPort;
		
    	try{
    		//TODO
    		bw = new BufferedWriter(new FileWriter(filePath+".txt"));
    		
    		for(int i=0;i<channels.size();i++){
    			numberOfAllDatas+=numberOfChannelDatas(channels.get(i).getWaveElements());
    		}	
    		bw.write(numberOfAllDatas+channels.size()+1+1+"\n");
    		bw.write(channels.size()+"\n");
    		
    		for(int i=0;i<channels.size();i++){
    			writeChannel(channels.get(i));
    		}
    		
		} catch (IOException e) {
		        e.printStackTrace();
		}finally {
			  if (null != bw) {
			       try {
			    	   bw.flush();
			    	   bw.close();
			    	   bw = null;
					} catch (IOException e) {
						e.printStackTrace();
					}
			   }
		}
    	
	}
	
	private String getCommand(){
		String command=MainWindow.startingDirectory+"\\wavdrive.exe "+
				MainWindow.startingDirectory+directoryName+defaultFileName+".txt"+" -r"+Protocol.getResolutionBit()+" -p"+arduinoPort+" -d";
		return command;
	}
	
	public void callWaveDrive(){
		System.out.println(getCommand());
		
		try {
			Runtime.getRuntime().exec(getCommand());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private int numberOfChannelDatas(LinkedList<AbstractWaveForm> waveElements){
		int counter=0;
		for(int i=0;i<waveElements.size();i++){
			counter+=waveElements.get(i).getCodedLength();
		}
		return counter+1;//+1 the channel identifier
	}
	
	
	private void writeChannel(Channel channel) throws IOException{
		LinkedList<AbstractWaveForm> waveElements=channel.getWaveElements();
		int numberOfDatas=numberOfChannelDatas(waveElements);	
		bw.write(numberOfDatas+"\n");
		bw.write(channel.getChannelNumber()+"\n");
		
		for(int i=0;i<waveElements.size();i++){
			
			AbstractWaveForm waveForm=waveElements.get(i);
			waveForm.createOutput(bw,channel.getMinValue(),channel.getMaxValue(),Double.parseDouble(Protocol.getPWMRange().toString()));
		}

	}
	

}
