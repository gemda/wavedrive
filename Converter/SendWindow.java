package Converter;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.LinkedList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import DataStructure.Channel;
import DataStructure.Protocol;
import Exception.WavedriveException;
import Main.MainWindow;
import OptionWords.Options;
import ReusableModul.FileChooser;

@SuppressWarnings("serial")
public class SendWindow extends JFrame implements ActionListener{


	
	public Object lock=new Object();
	
	private JPanel portPanel=new JPanel();
	private JPanel channelAssociationPanel=new JPanel();
	private JPanel buttonPanel=new JPanel();
	private FileChooser fileChooser=new FileChooser(Options.Save,Converter.directoryName,null);
	
	private LinkedList<JSpinner> spinners=new LinkedList<>();
	private JSpinner portSpinner;
	
	private Protocol protocol;
	private LinkedList<Channel> channels=new LinkedList<>();
	
	
	public SendWindow(Protocol _protocol) {
		protocol=_protocol;
		channels=protocol.getChannels();
		
		setLayout(new BorderLayout());
		
		setPortPanel();
		setChannelAssociationPanel();
		setButtonPanel();
		
		setWindowClosingOperation();
		setTitle("Sending channels to Arduino");
		setVisible(true);
		pack();	
	}
	
	/**
	 * Set window closing operation. Notify other frame waiting for this one
	 */
	public void setWindowClosingOperation(){
		this.addWindowListener(new WindowAdapter() {
	         @Override
	         public void windowClosing(WindowEvent evt) {
	             synchronized (lock) {
	                 setVisible(false);
	                 dispose();
	                 lock.notify();
	             }
	         }
	      });
	}
	
	private void setPortPanel(){
		JLabel portLabel=new JLabel("Arduino port");
		portPanel.add(portLabel);
		SpinnerModel portSpinnerModell=new SpinnerNumberModel(1,1,15,1);
		portSpinner=new JSpinner(portSpinnerModell);
		portPanel.add(portSpinner);
		this.add(portPanel,BorderLayout.NORTH);
	}
	
	private void setChannelAssociationPanel(){
		channelAssociationPanel.setLayout(new BoxLayout(channelAssociationPanel, BoxLayout.Y_AXIS));
		
		for(int i=0;i<channels.size();i++){
			JPanel panel=new JPanel();
			JLabel label=new JLabel("Channel "+i);
			SpinnerModel channelSpinnerModell=new SpinnerNumberModel(1,1,15,1);
			JSpinner channelSpinner=new JSpinner(channelSpinnerModell);
			spinners.add(channelSpinner);
			panel.add(label);
			panel.add(channelSpinner);
			channelAssociationPanel.add(panel);
		}

		this.add(channelAssociationPanel,BorderLayout.CENTER);
	}

	private void setButtonPanel(){
		JButton sendButton=new JButton("save");
		sendButton.setActionCommand(Options.ChooseFile);
		sendButton.addActionListener(this);
		buttonPanel.add(sendButton);
		
		JButton saveAndSendButton=new JButton("Send");
		saveAndSendButton.setActionCommand(Options.Send);
		saveAndSendButton.addActionListener(this);
		buttonPanel.add(saveAndSendButton);
		this.add(buttonPanel,BorderLayout.SOUTH);
	}

	private void callConverter(String filePath,int port, boolean sendToArduino){
		for(int i=0;i<spinners.size();i++){
			int channelNumber=Integer.parseInt(spinners.get(i).getValue().toString());
			channels.get(i).setChannelNumber(channelNumber);
		}
		
		protocol.paddChannels();
		
		Converter converter=new Converter(protocol,MainWindow.startingDirectory+Converter.directoryName+Converter.defaultFileName,port);
		
		if(sendToArduino){
			converter.callWaveDrive();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent action) {
		try {
			Object obj=action.getSource();
			if(obj.getClass()==JButton.class){
				
				String optionType=action.getActionCommand();
				
				Integer port=Integer.parseInt(portSpinner.getValue().toString());
				if(optionType.equals(Options.ChooseFile)){
					
					fileChooser.appearWindow();
					callConverter(fileChooser.getFilePath(),port, false);
					
				}else if(optionType.equals(Options.Send)){
					
					callConverter(MainWindow.startingDirectory+Converter.directoryName+Converter.defaultFileName,port,true);
					
				}else{
					throw new WavedriveException("Not recognizable command");
				}	
				
			}
		}catch(WavedriveException e){
			System.out.println("Problem in action handling  ");
			e.printStackTrace();
		}
		dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}
	
}
